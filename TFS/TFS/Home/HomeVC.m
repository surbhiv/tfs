//
//  HomeVC.m
//  Mother Dairy
//
//  Created by Surbhi on 24/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import "HomeVC.h"
#import <SSKeychain.h>
#import <Reachability.h>
#import <SVProgressHUD.h>
#import <ASIHTTPRequest.h>
#import <UIImageView+WebCache.h>
#import "PresentingAnimationController.h"
#import "DismissingAnimationController.h"
#import "VerticalSwipeInteractionController.h"
#import "HelperClass.h"
#import "NSString+PMUtils.h"
#import "AppDelegate.h"
#import "SplashViewController.h"
#import "MenuItemsTVC.h"
#import "TFS.pch"


@interface HomeVC ()<UICollectionViewDelegateFlowLayout>
{
    SplashViewController *splashController;
    NSMutableArray *arrCoupons;
    NSMutableArray *arrCouponsToDisplay;
    NSMutableArray *arrRewards;
    NSMutableArray *arrLoyalty;
    NSMutableArray *arrOffers;

    UIRefreshControl *refreshControl;
    UIActivityIndicatorView *refreshActivityView;
    UIBarButtonItem *rightButton;
    UIButton* right_button;
    NSTimer *timer;
    CGFloat lastContentOffset;
}
@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (appDelegate.isLoader == YES && appDelegate.viewOnceLoaded == NO) {
        splashController = [self.storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
        [appDelegate.window addSubview:splashController.view];
    }
    else {
        [UIApplication sharedApplication].statusBarHidden = NO;
        [appDelegate startIndicator];
    }
    
    if (appDelegate.viewOnceLoaded == NO) {
        appDelegate.viewOnceLoaded = YES;
    }
    
    if (appDelegate.justLogedIn == YES) {
        // show Alert
        NSString *name = [[NSUserDefaults standardUserDefaults]valueForKey:@"UserName"];
        [appDelegate showWelcomePop:name];
        appDelegate.justLogedIn = NO;
    }
    
    self.navigationController.navigationBar.tintColor = THEMECOLOR;
}

-(void)viewDidAppear:(BOOL)animated{
    
    NSLog(@"STEP 3");
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loadOffers) name:@"BACKLOGUpdated" object:nil];
    [appDelegate loadFeedbackBacklogValue];
}

-(void)viewDidDisappear:(BOOL)animated{
    [self removeTimer];
}


-(void)setUpView{

    [appDelegate stopIndicator];
    [refreshControl endRefreshing];
    [splashController.view removeFromSuperview];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    
    arrCouponsToDisplay = [[NSMutableArray alloc] init];

    arrCoupons = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in [appDelegate.dicOffers objectForKey:@"Coupons"]) {
        [arrCoupons addObject:dic];
        [arrCouponsToDisplay addObject:dic];
    }

    arrRewards = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in [appDelegate.dicOffers objectForKey:@"Rewards"]) {
        [arrRewards addObject:dic];
        [arrCouponsToDisplay addObject:dic];
    }
    
    
    
    arrLoyalty = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in [appDelegate.dicOffers objectForKey:@"Loyalty"]) {
        [arrLoyalty addObject:dic];

    }

    arrOffers = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in [appDelegate.dicOffers objectForKey:@"Offers"]) {
        [arrOffers addObject:dic];
    }

    if (arrOffers.count > 0) {
        [self.offerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrOffers objectAtIndex:0] objectForKey:@"ThumbUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            if (image && cacheType == SDImageCacheTypeNone) {
                self.offerImageView.alpha = 0.0;
                self.offerImageView.contentMode = UIViewContentModeScaleAspectFit;
                [UIView animateWithDuration:3.0 animations:^{
                    self.offerImageView.alpha = 1.0;
                }];
            }
        }];
        NSLog(@"%@",[[arrOffers objectAtIndex:0] valueForKey:@"Description"]);
        
        self.OfferDescriptionLabel.text = [[arrOffers objectAtIndex:0] valueForKey:@"Description"];
        [self.goToOfferButton setTitle:@"GO!" forState:UIControlStateNormal];
    }
    else if(appDelegate.backlogVal > 0){
        self.offerImageView.image = [UIImage imageNamed:@"FeedbackStar"];
        NSDictionary *attribute1 = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:GeorgeBold size:16.0]};
        NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Rate your shopping experience!" attributes:attribute1];
        NSMutableAttributedString * finalStr = [[NSMutableAttributedString alloc] initWithAttributedString:str1];
        
        [self.OfferDescriptionLabel setAttributedText: finalStr];
        self.offerImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.goToOfferButton setTitle:@"RATE NOW" forState:UIControlStateNormal];

    }
    else{
        self.offerImageView.image = [UIImage imageNamed:@"referHomeScreen"];
        NSDictionary *attribute1 = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:GeorgeBold size:16.0]};
        NSDictionary *attribute2 = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:GeorgeRegular size:14.0]};

        NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@" Refer a friend\n" attributes:attribute1];
        NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@" and win exciting rewards!" attributes:attribute2];

        NSMutableAttributedString * finalStr = [[NSMutableAttributedString alloc] initWithAttributedString:str1];
        [finalStr appendAttributedString:str2];
        
        [self.OfferDescriptionLabel setAttributedText: finalStr];
        self.offerImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.goToOfferButton setTitle:@"CLICK HERE" forState:UIControlStateNormal];
    }
    
    _pageController.numberOfPages = arrCouponsToDisplay.count;
    _pageController_Loyalty.numberOfPages = arrLoyalty.count;
    
    [self.gridCoupon reloadData];
    
    if (arrCouponsToDisplay.count > 1) {
//        int MaxSections = (int)arrCouponsToDisplay.count * 1000;
        int index = (int)arrCouponsToDisplay.count * 100;//MaxSections/(int)arrCouponsToDisplay.count;
        
        [self.gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0 ] atScrollPosition:UICollectionViewScrollPositionLeft animated:false];
        lastContentOffset = self.gridCoupon.contentOffset.x;

        _pageController.currentPage = 0;
        [self addTimer];
    }
    [self.gridLoyalty reloadData];
    [self UpdateSideMenuTable];
}


-(void)UpdateSideMenuTable{
    
    NSInteger numberOfCoupons = [[NSUserDefaults standardUserDefaults] integerForKey:@"COUPONCOUNT"];
    NSInteger numberOfLoaylty = [[NSUserDefaults standardUserDefaults] integerForKey:@"LOYALTYCOUNT"];
    NSInteger numberOfRewards = [[NSUserDefaults standardUserDefaults] integerForKey:@"REWARDSCOUNT"];
    NSInteger numberOfOffers = [[NSUserDefaults standardUserDefaults] integerForKey:@"OFFERSCOUNT"];

    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
    mo.arrCouponCount = (int)[[appDelegate.dicOffers objectForKey:@"Coupons"] count] - (int)numberOfCoupons;
    mo.arrRewardsCount = (int)[[appDelegate.dicOffers objectForKey:@"Rewards"] count] - (int)numberOfRewards;
    mo.arrLoyaltyCount = (int)[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] - (int)numberOfLoaylty;
    mo.arrOffersCount = (int)[[appDelegate.dicOffers objectForKey:@"Offers"] count] - (int)numberOfOffers;
    mo.feedbackCount = appDelegate.backlogVal;
    
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] forKey:@"LOYALTYCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Rewards"] count] forKey:@"REWARDSCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Coupons"] count] forKey:@"COUPONCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Offers"] count] forKey:@"OFFERSCOUNT"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    [mo.tableView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    if (!appDelegate.isRegistered && appDelegate.strDeviceToken.length > 0) {
        NSLog(@"\n\n\nDEVICE REGISTER API\n\n\n");

        appDelegate.strUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        appDelegate.strMobileNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"MSN"];
        [appDelegate registerDevice];
    }
}

- (IBAction)toggleSideMenuView:(id)sender
{
    [self.navigationController toggleSideMenuView];
}

#pragma mark -
#pragma mark Custom Methods

- (void)loadOffers {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@coupons/%@/true", BASEURL, appDelegate.strUserId];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        NSLog(@"%@",_dateString);
        NSLog(@"%@",appDelegate.strUserId);
        NSLog(@"%@",signature);
        
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:3000.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        [request startAsynchronous];
    }
    else {
        
        [refreshControl endRefreshing];
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [refreshControl endRefreshing];
    [splashController.view removeFromSuperview];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    appDelegate.dicOffers = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [self setUpView];
}



- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [refreshControl endRefreshing];
    [splashController.view removeFromSuperview];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    _pageController.numberOfPages = 0;
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];
}

#pragma mark
#pragma mark - FlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE_X) {
        self.CollectionHeight.constant = 269;
        return CGSizeMake(collectionView.bounds.size.width, 269);// 249.5
    }
    else if (IS_IPHONE_6P) {
        self.CollectionHeight.constant = 300;
        return CGSizeMake(collectionView.bounds.size.width, 300); //275
    }
    else if (IS_IPHONE_6){
        self.CollectionHeight.constant = 269;
        return CGSizeMake(collectionView.bounds.size.width, 269);
    }
    else if (IS_IPHONE_5)
    {
        self.CollectionHeight.constant = 212.5;
        return CGSizeMake(collectionView.bounds.size.width, 212.5);
    }
    
    return CGSizeMake(collectionView.bounds.size.width, ([UIScreen mainScreen].bounds.size.height - 105)/2);

}


#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([collectionView isEqual:self.gridCoupon]) {
        if (arrCouponsToDisplay.count == 1) {
            return 1;
        }
        return arrCouponsToDisplay.count * 1000 ;
    }
    else{
        if (arrLoyalty.count > 0) {
            return 1;
        }
        return arrLoyalty.count;
    }
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier;
    if ([collectionView isEqual:self.gridCoupon]) {
        cellIdentifier = @"CouponCellGRID";
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1200];
        
        int index;

        if (arrCouponsToDisplay.count == 1) {
            index = 0;
        }
        else {
            index = (int)indexPath.row % arrCouponsToDisplay.count;
        }
        
        if ([[arrCouponsToDisplay objectAtIndex:index] objectForKey:@"ThumbUrl"] == nil) {
            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrCouponsToDisplay objectAtIndex:index] objectForKey:@"TopCouponImageUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (image && cacheType == SDImageCacheTypeNone) {
                    imageView.alpha = 0.0;
                    [UIView animateWithDuration:3.0 animations:^{
                        imageView.alpha = 1.0;
                    }];
                }
            }];
        }
        else{
            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrCouponsToDisplay objectAtIndex:index] objectForKey:@"ThumbUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (image && cacheType == SDImageCacheTypeNone) {
                    imageView.alpha = 0.0;
                    [UIView animateWithDuration:3.0 animations:^{
                        imageView.alpha = 1.0;
                    }];
                }
            }];
        }
        
        
        return cell;
    }
    else{
        cellIdentifier = @"LoyalityCellGRID";
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1201];        
        
        int index;
        if (arrLoyalty.count == 1) {
            index = 0;
        }
        else {
            index = (int)indexPath.row ;
        }

        if ([[arrLoyalty objectAtIndex:index] objectForKey:@"ThumbUrl"] == nil) {
            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrLoyalty objectAtIndex:index] objectForKey:@"TopCouponImageUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (image && cacheType == SDImageCacheTypeNone) {
                    imageView.alpha = 0.0;
                    [UIView animateWithDuration:3.0 animations:^{
                        imageView.alpha = 1.0;
                    }];
                }
            }];
        }
        else{
            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrLoyalty objectAtIndex:index] objectForKey:@"ThumbUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (image && cacheType == SDImageCacheTypeNone) {
                    imageView.alpha = 0.0;
                    [UIView animateWithDuration:3.0 animations:^{
                        imageView.alpha = 1.0;
                    }];
                }
            }];
        }
        
        
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView isEqual:self.gridCoupon]) {
        
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        int selecetdIndex;
        
        if (indexPath.item >= arrCouponsToDisplay.count) {
            int newIndex = ((int)indexPath.item) % arrCouponsToDisplay.count;
            selecetdIndex = newIndex;
        }
        else{
            selecetdIndex = (int)indexPath.item;
        }
        
        if (selecetdIndex < arrCoupons.count) {
            mo.selectedPageOnDetail = selecetdIndex;
            [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        }
        else{
            mo.selectedPageOnDetail = selecetdIndex - (int)arrCoupons.count;
            [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        }
    }
    else{
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        mo.selectedPageOnDetail = (int)indexPath.item;

        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    }
}


//MARK: - AUTO SCROLLING
-(void)addTimer{
    
    if (timer) {
        [timer invalidate];
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(nextPage) userInfo:nil repeats:true];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

-(void)nextPage{
    
    int MaxSections;
    if (arrCouponsToDisplay.count == 1) {
        MaxSections = (int)arrCouponsToDisplay.count;
    }
    else {
        MaxSections = (int)arrCouponsToDisplay.count * 1000;
    }
    lastContentOffset = self.gridCoupon.contentOffset.x;
    
    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    
    if (ar.count > 0) {
        NSIndexPath *currentIndexPathReset = ar[0];
        
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = [NSIndexPath indexPathForItem:0 inSection:0];//NSIndexPath(forItem:0, inSection:0)
            _pageController.currentPage = 0;
            [_gridCoupon scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionRight animated:false];
        }
        else
        {
            int newIndex = ((int)currentIndexPathReset.item + 1) % arrCouponsToDisplay.count;
            int newIndex2 = ((int)currentIndexPathReset.item + 1);

            _pageController.currentPage = newIndex;
            [_gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:newIndex2 inSection:0 ] atScrollPosition:UICollectionViewScrollPositionLeft animated:true];
        }
    }
}






-(void)removeTimer
{
    [timer invalidate];
}

// UIScrollView' delegate method
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self removeTimer];
}



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    
    if (lastContentOffset < scrollView.contentOffset.x) {
        [self CalculateNextPage];
    }
    else{
        if (ar.count > 1) {
            NSIndexPath *currentIndexPathReset = ar[0];
            NSIndexPath *newIndexPathReset = ar[1];
            
            int oldIndex = ((int)currentIndexPathReset.item) % arrCouponsToDisplay.count;
            int newIndex = ((int)newIndexPathReset.item) % arrCouponsToDisplay.count;
            
            int ind = MIN(oldIndex, newIndex);
            
            if (ind ==  newIndex) {
                // previous
                _pageController.currentPage = newIndex;
                int newIndexToScroll = ((int)newIndexPathReset.item);
                
                
                [_gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:newIndexToScroll inSection:0 ] atScrollPosition:UICollectionViewScrollPositionRight animated:false];
            }
            else{
                _pageController.currentPage = oldIndex;
                int newIndexToScroll = ((int)currentIndexPathReset.item);
                
                
                [_gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:newIndexToScroll inSection:0 ] atScrollPosition:UICollectionViewScrollPositionRight animated:false];
            }
        }
    }
    
    lastContentOffset = scrollView.contentOffset.x;
    
    
    if (arrCouponsToDisplay.count > 1) {
        [self addTimer];
    }
}


-(void)CalculateNextPage{
    int MaxSections;
    if (arrCouponsToDisplay.count == 1) {
        MaxSections = (int)arrCouponsToDisplay.count;
    }
    else {
        MaxSections = (int)arrCouponsToDisplay.count * 1000;
    }

    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    if (ar.count > 0) {
        NSIndexPath *currentIndexPathReset = ar[0];
        
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = [NSIndexPath indexPathForItem:0 inSection:0];
            _pageController.currentPage = 0;
        }
        else
        {
            int newIndex = ((int)currentIndexPathReset.item + 1) % arrCouponsToDisplay.count;
            _pageController.currentPage = newIndex;
        }
    }

}

-(void)CalculatePreviousPage{
    int MaxSections;
    if (arrCouponsToDisplay.count == 1) {
        MaxSections = (int)arrCouponsToDisplay.count;
    }
    else {
        MaxSections = (int)arrCouponsToDisplay.count * 1000;
    }
    
    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    if (ar.count > 0) {
        NSIndexPath *currentIndexPathReset = ar[0];
        
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = [NSIndexPath indexPathForItem:0 inSection:0];
            _pageController.currentPage = 0;
        }
        else
        {
            int newIndex = ((int)currentIndexPathReset.item + 1) % arrCouponsToDisplay.count;
            _pageController.currentPage = newIndex;
        }
    }
    
}


#pragma mark -
#pragma mark UIBarButtoItem Methods


- (IBAction)RfreshButtonPressed:(id)sender {
    [self removeTimer];
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    [self.imgHeader addSubview:refreshActivityView];

    
    _btnRefresh.hidden = YES;
    [appDelegate startIndicator];
    [appDelegate loadFeedbackBacklogValue];
}

- (IBAction)GoToOfferButtonPressed:(id)sender;
{
    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];

    if (arrOffers.count > 0){
        
        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];

//        CouponDetailsViewController *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"detailsController"];
//        dashboard.title = @"OFFERS";
//        
//        if ([[appDelegate returnTopViewController] isKindOfClass:[CouponDetailsViewController class]]) {
//            CouponDetailsViewController *temp = [appDelegate returnTopViewController];
//            temp.title = @"OFFERS";
//            [temp viewDidLoad];
//            [appDelegate setTopViewwithVC:temp];
//        }
//        else {
//            [appDelegate setTopViewwithVC:dashboard];
//        }
//        
//        mo.selectedIndexPath = [NSIndexPath indexPathForRow:10 inSection:0];

    }
    else if(appDelegate.backlogVal > 0){
        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    }
    else{
        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
        mo.LastselectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
@end
