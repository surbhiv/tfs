//
//  PresentingAnimationController.h
//  RConnect
//
//  Created by Avineet on 24/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "VerticalSwipeInteractionController.h"

@interface PresentingAnimationController : VerticalSwipeInteractionController < UIViewControllerAnimatedTransitioning > {
    
    BOOL isPresenting;
}

@property (nonatomic, assign) CGRect initialframe;

@property (nonatomic, assign) BOOL isCoupon;

@property (nonatomic, strong) UIView * startingView;

@end
