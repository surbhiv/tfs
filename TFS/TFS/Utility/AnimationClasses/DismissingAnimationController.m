//
//  DismissingAnimationController.m
//  RConnect
//
//  Created by Avineet on 24/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "DismissingAnimationController.h"

//#import "CouponsViewController.h"
//#import "LoyaltyViewController.h"
//#import "CouponDetailsViewController.h"

@implementation DismissingAnimationController

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.80;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
//    CouponDetailsViewController *fromVC = (CouponDetailsViewController *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    
//    UIViewController * toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
//    
//    UIView *toView = self.toVieww;
//    UIView *fromView = fromVC.view;
//    
//    UIView* containerView = [transitionContext containerView];
//    [containerView addSubview:toVC.view];
//    [containerView sendSubviewToBack:toVC.view];
//    
//    // -- Add a clean background on cell
//    UIView * backGroundView = [[UIView alloc]
//                               initWithFrame:CGRectMake(8, self.finishRect.origin.y + 72,
//                                                                       SCREEN_WIDTH - 16,
//                                                                       self.finishRect.size.height - 8)];
//    backGroundView.backgroundColor = [UIColor whiteColor];
//    backGroundView.opaque = YES;
//    backGroundView.layer.opaque = YES;
//    [containerView addSubview:backGroundView];
//    
//    // Add a perspective transform
//    CATransform3D transform = CATransform3DIdentity;
//    transform.m34 = -0.002;
//    [containerView.layer setSublayerTransform:transform];
//    
//    // Give both VCs the same start frame
//    CGRect initialFrame = [transitionContext initialFrameForViewController:fromVC];
//    toView.frame =  CGRectMake(8, self.finishRect.origin.y + 64 + 8, SCREEN_WIDTH - 16, self.finishRect.size.height - 8);
//    fromView.frame =  initialFrame;
//        
//    // customise
//    CGRect snapshotRegion = CGRectMake(8,
//                                       self.finishRect.origin.y + 64 + 8,
//                                       SCREEN_WIDTH - 16,
//                                       self.finishRect.size.height - 8);
//    
//    UIView *leftHandView = [toVC.view resizableSnapshotViewFromRect:snapshotRegion
//                                                 afterScreenUpdates:NO
//                                                      withCapInsets:UIEdgeInsetsZero];
//    leftHandView.frame = snapshotRegion;
//    [containerView addSubview:leftHandView];
//    
//    [containerView sendSubviewToBack:leftHandView];
//    
//    // create two-part snapshots of both the from- and to- views
//    NSArray* toViewSnapshots = @[leftHandView];
//    
//    UIView* flippedSectionOfToView = toViewSnapshots[0];
//    
//    NSArray* fromViewSnapshots = [self createSnapshots:fromView afterScreenUpdates:NO];
//    
//    UIView* flippedSectionOfFromView = [fromViewSnapshots objectAtIndex:1];
//    
//    // change the anchor point so that the view rotate around the correct edge
//    [self updateAnchorPointAndOffset:CGPointMake(0.5, 1.0) view:flippedSectionOfFromView];
//    [self updateAnchorPointAndOffset:CGPointMake(0.5, 0.0) view:flippedSectionOfToView];
//    
//    // rotate the to- view by 90 degrees, hiding it
//    flippedSectionOfToView.layer.transform = [self rotate:M_PI_2];
//    
//    flippedSectionOfToView.alpha = 0;
//    
//    NSTimeInterval duration = [self transitionDuration:transitionContext];
//    
//    [UIView animateKeyframesWithDuration:duration
//                                   delay:0.0
//                                 options:UIViewKeyframeAnimationOptionCalculationModeLinear
//                              animations:^{
//                                  
//            [UIView addKeyframeWithRelativeStartTime:0.0
//                                    relativeDuration:0.4
//                                          animations:^{
//                                              
//                                              flippedSectionOfFromView.layer.transform = [self rotate:-M_PI_2];
//                                              
//                                              flippedSectionOfToView.frame = CGRectMake(8, SCREEN_HEIGHT/2, SCREEN_WIDTH - 16, self.finishRect.size.height - 8);
//                                              
//                                              ((UIView *)fromViewSnapshots[0]).frame =CGRectMake(8, SCREEN_HEIGHT/2, SCREEN_WIDTH - 16, self.finishRect.size.height - 8);
//                                              
//                                              ((UIView *)fromViewSnapshots[1]).frame = CGRectMake(8, SCREEN_HEIGHT/2, SCREEN_WIDTH - 16, 0);
//                                          }];
//                                  
//                [UIView addKeyframeWithRelativeStartTime:0.4
//                                        relativeDuration:0.001
//                                              animations:^{
//                                                  flippedSectionOfToView.alpha = 1;
//                                        }];
//                                  
//                [UIView addKeyframeWithRelativeStartTime:0.401
//                                        relativeDuration:0.4
//                                              animations:^{
//                                                  
//                                                  flippedSectionOfToView.layer.transform = [self rotate:0.001];
//                                              }];
//                                  
//                [UIView addKeyframeWithRelativeStartTime:0.401
//                                        relativeDuration:0.45
//                                              animations:^{
//                                                  
//                                                  flippedSectionOfToView.frame = CGRectMake(8, self.finishRect.origin.y + 64 + 8, SCREEN_WIDTH - 16, self.finishRect.size.height - 8);
//                                                  
//                                                  ((UIView *)fromViewSnapshots[0]).frame=CGRectMake(8, self.finishRect.origin.y + 64 + 8, SCREEN_WIDTH - 16, self.finishRect.size.height - 8);
//                                                  
//                                                  ((UIView *)fromViewSnapshots[1]).frame=CGRectMake(8, self.finishRect.origin.y + 64 + 8, SCREEN_WIDTH - 16, 0);
//                                                                
//                                            }];
//                                  
//                                  
//                              } completion:^(BOOL finished) {
//                                  
//                                  // remove all the temporary views
//                                  if ([transitionContext transitionWasCancelled]) {
//                                      [self removeOtherViews:fromView];
//                                  } else {
//                                      [self removeOtherViews:toView];
//                                      if (APP_DELEGATE.settingsInteractionController) {
//                                          [APP_DELEGATE.settingsInteractionController wireToViewController:toVC withView:toView forOperation:InteractionOperationPresent];
//                                      }
//                                  }
//                                  // inform the context of completion
//                                  [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
//                              }];
}

- (NSArray*)createSnapshots:(UIView*)view afterScreenUpdates:(BOOL) afterUpdates {
    
    UIView* containerView = view.superview;
    CGRect snapshotRegion;
    
    // snapshot the left-hand side of the view
    snapshotRegion = CGRectMake(0,view.center.y,view.frame.size.width , view.frame.size.height/2);
    
    //issue in iPhone 6
    UIView *leftHandView = [view resizableSnapshotViewFromRect:CGRectMake(0,view.frame.size.height/2,view.frame.size.width,view.frame.size.height/2)  afterScreenUpdates:afterUpdates withCapInsets:UIEdgeInsetsZero];
    leftHandView.frame = snapshotRegion;
    [containerView addSubview:leftHandView];
    
    // snapshot the right-hand side of the view
    snapshotRegion = CGRectMake(0,0,view.frame.size.width , view.frame.size.height/2);
    
    
    UIView *rightHandView = [view resizableSnapshotViewFromRect:CGRectMake(0, 0,view.frame.size.width,view.frame.size.height/2)  afterScreenUpdates:afterUpdates withCapInsets:UIEdgeInsetsZero];
    rightHandView.frame = snapshotRegion;
    [containerView addSubview:rightHandView];
    
    // send the view that was snapshotted to the back
    [containerView sendSubviewToBack:view];
    
    return @[leftHandView, rightHandView];
}

- (void)removeOtherViews:(UIView*)viewToKeep {
    UIView* containerView = viewToKeep.superview;
    for (UIView* view in containerView.subviews) {
        if (view != viewToKeep) {
            [view removeFromSuperview];
        }
    }
}

- (void)updateAnchorPointAndOffset:(CGPoint)anchorPoint view:(UIView*)view {
    
    view.layer.anchorPoint = anchorPoint;
    float xOffset =  anchorPoint.y - 0.5;
    
    view.frame = CGRectOffset(view.frame, 0, xOffset * view.frame.size.height);
}

- (CATransform3D) makeRotationAndPerspectiveTransform:(CGFloat) angle {
    CATransform3D transform = CATransform3DMakeRotation(angle, 1.0f, 0.0f, 0.0f);
    transform.m34 = 1.0 / -500;
    return transform;
}

- (CATransform3D) rotate:(CGFloat) angle {
    return  CATransform3DMakeRotation(angle, 1.0, 0.0, 0.0);
}

@end
