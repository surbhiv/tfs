//
//  BaseInteractionController.h
//  RConnect
//
//  Created by Avineet on 24/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, InteractionOperation) {
    
    //Indicates that the interaction controller should start a navigation controller 'pop' navigation.
    InteractionOperationPop,
    
    //Indicates that the interaction controller should initiate a modal 'dismiss'.
    InteractionOperationDismiss,
    
    //Indicates that the interaction controller should navigate between tabs.
    InteractionOperationTab,
    
    InteractionOperationPresent
};


@interface BaseInteractionController : UIPercentDrivenInteractiveTransition

- (void)wireToViewController:(UIViewController*)viewController forOperation:(InteractionOperation)operation;


- (void)wireToViewController:(UIViewController*)viewController withView:(UIView*)viewT forOperation:(InteractionOperation)operation;

@property (nonatomic, assign) BOOL interactionInProgress;

@end
