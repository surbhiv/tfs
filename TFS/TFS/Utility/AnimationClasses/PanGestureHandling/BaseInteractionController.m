//
//  BaseInteractionController.m
//  RConnect
//
//  Created by Avineet on 24/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "BaseInteractionController.h"

@implementation BaseInteractionController

- (void)wireToViewController:(UIViewController *)viewController forOperation:(InteractionOperation)operation {
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    
}

- (void)wireToViewController:(UIViewController*)viewController withView:(UIView*)viewT forOperation:(InteractionOperation)operation{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];

}

@end
