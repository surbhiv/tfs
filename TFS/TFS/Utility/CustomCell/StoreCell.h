//
//  StoreCell.h
//  RConnect
//
//  Created by Hitesh Dhawan on 08/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

@property (weak, nonatomic) IBOutlet UILabel *storeName;
@property (weak, nonatomic) IBOutlet UILabel *storeDistance;
@property (weak, nonatomic) IBOutlet UILabel *storeState;
@property (weak, nonatomic) IBOutlet UILabel *storePhone;
@property (weak, nonatomic) IBOutlet UILabel *viewOnMap;
@property (weak, nonatomic) IBOutlet UIImageView *storeImage;

@end
