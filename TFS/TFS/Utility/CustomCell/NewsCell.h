//
//  NewsCell.h
//  RConnect
//
//  Created by Hitesh Dhawan on 08/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgNewsImage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPublishDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImg;

@end
