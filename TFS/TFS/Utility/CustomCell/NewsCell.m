//
//  NewsCell.m
//  RConnect
//
//  Created by Hitesh Dhawan on 08/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "NewsCell.h"

@implementation NewsCell

- (void)awakeFromNib {
    // Initialization code
    
    _arrowImg.image = [[UIImage imageNamed:@"read-more"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _arrowImg.tintColor = [UIColor colorWithRed:0.0/255.0 green:96.0/255.0 blue:137.0/255.0 alpha:1];
}

-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
}


@end
