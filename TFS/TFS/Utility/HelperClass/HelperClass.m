//
//  HelperClass.m
//  LiQUiD BARCODES
//
//  Created by HiteshDhawan on 23/12/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "HelperClass.h"

@implementation HelperClass

+ (NSString *)getDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // ISO-8601 format
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    return [formatter stringFromDate:[NSDate date]];
}

+ (NSString *)GetGUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

+ (BOOL) isValidEmail:(NSString *)emailString {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];
}




@end
