//
//  HelperClass.h
//  LiQUiD BARCODES
//
//  Created by HiteshDhawan on 23/12/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelperClass : NSObject

+ (NSString *)getDateString;
+ (NSString *)GetGUID;
+ (BOOL) isValidEmail:(NSString *)emailString;
@end
