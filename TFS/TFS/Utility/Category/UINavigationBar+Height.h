//
//  UINavigationBar+Height.h
//  J Mart
//
//  Created by Avineet on 17/05/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Height)

- (CGSize)sizeThatFits:(CGSize)size;

@end
