//
//  UIImageView+RenderingMode.m
//  J Mart
//
//  Created by Avineet on 17/05/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

#import "UIImageView+RenderingMode.h"

@implementation UIImageView (RenderingMode)

- (void)setImageRenderingMode:(UIImageRenderingMode)renderMode {
    
    NSAssert(self.image, @"Image must be set before setting rendering mode");
    self.image = [self.image imageWithRenderingMode:renderMode];
}

@end
