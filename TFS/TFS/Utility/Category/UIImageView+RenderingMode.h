//
//  UIImageView+RenderingMode.h
//  J Mart
//
//  Created by Avineet on 17/05/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (RenderingMode)

- (void)setImageRenderingMode:(UIImageRenderingMode)renderMode;

@end
