//
//  DateTimePicker.m
//  CustomDateTimePicker
//
//  Created by Coder on 4/2/14.
//  Copyright (c) 2014 AcademicSoftware. All rights reserved.
//

#import "DateTimePicker.h"

#define MyDateTimePickerToolbarHeight 50

@interface DateTimePicker()

@property (nonatomic, assign, readwrite) UIDatePicker *picker;

@property (nonatomic, assign) id doneTarget;
@property (nonatomic, assign) id cancelTarget;
@property (nonatomic, assign) SEL doneSelector;
@property (nonatomic, assign) SEL cancelSelector;

- (void) donePressed;

@end


@implementation DateTimePicker

@synthesize picker = _picker;
@synthesize doneTarget = _doneTarget;
@synthesize cancelTarget = _cancelTarget;
@synthesize doneSelector = _doneSelector;
@synthesize cancelSelector = _cancelSelector;

- (id) initWithFrame: (CGRect) frame {
    
    if ((self = [super initWithFrame: frame])) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame: CGRectMake(0, MyDateTimePickerToolbarHeight, frame.size.width, frame.size.height - MyDateTimePickerToolbarHeight)];
        [self addSubview: picker];
        
        picker.maximumDate = [NSDate date];
        // Set toolbar backgoud color (Just for giving black background)
        UIView *toolBarBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, MyDateTimePickerToolbarHeight)];
        toolBarBackground.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:44.0/255.0 blue:42.0/255.0 alpha:1.0];
        [self addSubview:toolBarBackground];
        
        // Create toolbar and add subitems on it
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, frame.size.width, MyDateTimePickerToolbarHeight)];
        toolbar.barStyle = UIBarStyleBlackOpaque;
        toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(donePressed)];
        UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle: @"Cancel" style: UIBarButtonItemStylePlain target: self action: @selector(cancelPressed)];
        toolbar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
        
        
        
        [self addSubview: toolbar];
        
        self.picker = picker;
        picker.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        
        self.autoresizesSubviews = YES;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    }
    return self;
}

- (void) setMode: (UIDatePickerMode) mode
{
    if (mode == nil) {
        self.picker.datePickerMode = UIDatePickerModeDate;
    }
    self.picker.datePickerMode = mode;
}

- (void) donePressed
{
    if (self.doneTarget)
    {
        [self.doneTarget performSelector:self.doneSelector withObject:nil afterDelay:0];
    }
}

- (void) cancelPressed
{
    if (self.cancelTarget) {
        [self.cancelTarget performSelector:self.cancelSelector withObject:nil afterDelay:0];
    }
}

- (void) addTargetForDoneButton:(id) target action: (SEL) action {
    
    self.doneTarget = target;
    self.doneSelector = action;
}

- (void) addTargetForCancelButton: (id) target action: (SEL) action {
    
    self.cancelTarget=target;
    self.cancelSelector=action;
}

@end

