//
//  RegisterViewController.h
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFS.pch"

@interface RegisterViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSArray *arrCountryCode;

@property (weak, nonatomic) IBOutlet UITextField *txtPhoneCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtDateOfBirth;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckMark;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTerms;


- (IBAction)registerButtonPressed:(id)sender;
- (IBAction)checkMarkButtonPressed:(id)sender;




@end
