//
//  PinViewController.h
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFS.pch"

@interface PinViewController : UIViewController

@property (nonatomic, strong) NSDictionary *dicUserInfo;

@property (weak, nonatomic) IBOutlet UIView *viewPin;
@property (weak, nonatomic) IBOutlet UITextField *txtPin;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)submitButtonPressed:(id)sender;


@end
