//
//  RegisterViewController.m
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "RegisterViewController.h"
#import "UITextField+Padding.h"
#import "DateTimePicker.h"
#import <Reachability.h>
#import <SVProgressHUD.h>
#import <ASIHTTPRequest.h>
#import "PinViewController.h"
#import <SSKeychain.h>
#import "UIDevice+NTNUExtensions.h"
#import "HelperClass.h"
#import "NSString+PMUtils.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>


#define MyDateTimePickerHeight 260

@interface RegisterViewController () <TTTAttributedLabelDelegate>
{
    DateTimePicker *datePicker;
    UIPickerView *countryPicker;
    UIToolbar *toolBar;
}
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES];
    
    NSString * string = @"I agree to the Privacy Policy and T&C";
    
    self.lblTerms.text = string;
    
    self.lblTerms.linkAttributes = [NSDictionary dictionaryWithObjectsAndKeys:@(NSUnderlineStyleSingle), NSUnderlineStyleAttributeName, THEMECOLOR, NSForegroundColorAttributeName,[UIFont fontWithName:GeorgeSemiBold size:16.0],NSFontAttributeName, nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPinNumber) name:@"USERIDCREATED" object:nil];
    
    
    [self.lblTerms addLinkToURL:[NSURL URLWithString:@"www.google.com"]
                      withRange:NSMakeRange([string rangeOfString:@"Privacy Policy and T&C"
                                                          options:NSCaseInsensitiveSearch].location, @"Privacy Policy and T&C".length)];
    
    [self.lblTerms addLinkToURL:[NSURL URLWithString:@"www.apple.com"]
                      withRange:NSMakeRange([string rangeOfString:@"T&C"
                                                          options:NSCaseInsensitiveSearch].location, @"T&C".length)];
    
    [self.lblTerms setDelegate:self];
    self.lblTerms.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    
    
    // Create custom date picker
    datePicker = [[DateTimePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, MyDateTimePickerHeight)];
    [datePicker addTargetForDoneButton:self action:@selector(donePressed)];
    [datePicker addTargetForCancelButton:self action:@selector(cancelPressed)];
    [self.view addSubview:datePicker];
    datePicker.hidden = NO;
    [datePicker setMode:UIDatePickerModeDate];
    
    [datePicker.picker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];

    // Create country picker
//    [self addCountryPickerView];
    
    // Fetch list of phone code
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryPhoneCode" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    self.arrCountryCode = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] objectForKey:@"countries"];
    
    
    // Dismiss keyboard gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    [self setImageOntextField:self.txtMobileNumber andImage:@"mobile" withSize:CGSizeMake(30, 30)];
    [self setImageOntextField:self.txtDateOfBirth andImage:@"calender" withSize:CGSizeMake(30, 30)];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - DateTimePickerDelegate Methods

- (void)pickerChanged:(id)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    [self.txtDateOfBirth setText:[dateFormatter stringFromDate:datePicker.picker.date]];
}

- (void)donePressed {
    
    [UIView animateWithDuration:.5 animations:^{
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, MyDateTimePickerHeight)];
    } completion:^(BOOL finished) {
        datePicker.hidden = YES;
        //_viewDateofBirth.layer.borderColor=[UIColor clearColor].CGColor;
    }];
}

- (void)cancelPressed {
    
    [UIView animateWithDuration:.5 animations:^{
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, MyDateTimePickerHeight)];
    } completion:^(BOOL finished) {
        datePicker.hidden = YES;
    }];
    
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([textField isEqual:_txtPhoneCode]) {
        [self dismissKeyboard];
        if (textField.text.length == 0) textField.text = [[self.arrCountryCode firstObject] objectForKey:@"code"];;
        
        [self addCountryPickerView];
        [countryPicker reloadAllComponents];
        [countryPicker selectRow:[self getIndexOfValue:textField.text presentInArray:self.arrCountryCode forKey:@"code"] inComponent:0 animated:NO];
//        countryPicker.hidden = YES;
//        [self.view bringSubviewToFront:countryPicker];
    }
    else if ([textField isEqual:_txtDateOfBirth]){
        [self dismissKeyboard];
        datePicker.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^
         {
             [datePicker setFrame:CGRectMake(0, self.view.frame.size.height - MyDateTimePickerHeight, self.view.frame.size.width, MyDateTimePickerHeight)];
         }];

    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    if ([textField isEqual:_txtMobileNumber]) {
        [self.txtMobileNumber resignFirstResponder];
        [self.txtName becomeFirstResponder];
    }
    else if ([textField isEqual:self.txtName]) {
        [self.txtName resignFirstResponder];
        [self.txtDateOfBirth becomeFirstResponder];
    }
    else{
        [self dismissKeyboard];
    }
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_txtMobileNumber]) {
        if (range.location > 9) {
            return NO;
        }
    }
    return YES;
}

- (NSInteger) getIndexOfValue:(id) value presentInArray:(NSArray *)array forKey:(NSString *) key {
    
    NSInteger i = 0;
    
    for (i = 0; i < array.count; i++) {
        
        if ([array[i][key] isEqual:value]) {
            break;
        }
    }
    return i;
}

#pragma mark - UIPickerViewDataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.arrCountryCode count];
}

#pragma mark - UIPickerViewDelegate Methods

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

    return [NSString stringWithFormat:@"%@      %@", [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"], [[self.arrCountryCode objectAtIndex:row] objectForKey:@"name"]];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.txtPhoneCode.text = [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"];
}


#pragma mark - Custom Methods

- (void)dismissKeyboard {
    
    [self.txtMobileNumber resignFirstResponder];
    [self.txtName resignFirstResponder];
    [self.txtPhoneCode resignFirstResponder];
    [self.txtDateOfBirth resignFirstResponder];
}

- (void)addCountryPickerView {
    
    countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 200)];
    countryPicker.dataSource = self;
    countryPicker.delegate = self;
    countryPicker.backgroundColor = [UIColor whiteColor];
    countryPicker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - countryPicker.frame.size.height - 50, SCREEN_WIDTH, 50)];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    
    [self.view addSubview:countryPicker];
    [self.view addSubview:toolBar];
    
//    countryPicker.hidden = YES;
//    toolBar.hidden = YES;
    
    self.txtPhoneCode.inputView = countryPicker;
    self.txtPhoneCode.inputAccessoryView = toolBar;
    
    [self.view bringSubviewToFront:countryPicker];
    [self.view bringSubviewToFront:toolBar];
}

- (void)done:(id)sender {
    
    [self.txtPhoneCode resignFirstResponder];
    [countryPicker removeFromSuperview];
    [toolBar removeFromSuperview];
    countryPicker.hidden = YES;
    toolBar.hidden = YES;

}

- (void)getPinNumber {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [appDelegate startIndicator];
        
        NSString *urlString = [NSString stringWithFormat:@"%@pin", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobileNumber.text];
        appDelegate.strMobileNumber = strMobileNumber;
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, strMobileNumber, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSLog(@"GET PIN API - %@",appDelegate.strUserId);
        NSLog(@"%@",appDelegate.strUserId);
        
        
        if (appDelegate.strUserId.length > 0){
            NSDictionary *paramDic = @{@"UserID":appDelegate.strUserId,
                                       @"MSN":strMobileNumber};
            NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
            [request setPostBody:[NSMutableData dataWithData:data]];
            
            [request startAsynchronous];
        }
        else{
//            [self getPinNumber];
            [appDelegate getUser];
//            [self performSelector:@selector(getPinNumber) withObject:nil afterDelay:10.0];
            [appDelegate stopIndicator];
        }
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet."];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    NSDictionary *dicResponse = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicResponse objectForKey:@"Status"] isEqualToString:@"SmsSent"]) {
        
        NSMutableDictionary *dicInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:appDelegate.strMobileNumber, @"MSN", appDelegate.strUserId, @"UserId", self.txtName.text, @"UserName", nil];
        if (_txtDateOfBirth.text.length > 0) {
            [dicInfo setObject:_txtDateOfBirth.text forKey:@"DateOfBirth"];
        }
        
        PinViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PinViewController"];
        viewController.dicUserInfo = [dicInfo mutableCopy];
        [self presentViewController:viewController animated:true completion:nil];
//        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if ([[dicResponse objectForKey:@"success"] integerValue] == 0) {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"App is already registered from another number on this device. Please enter the correct number to proceed."];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription];
}

#pragma mark - IBAction Methods

- (IBAction)registerButtonPressed:(id)sender {
    
    if (self.txtName.text.length > 0 && self.txtMobileNumber.text.length > 0) {
        
        if (self.btnCheckMark.selected) {
            
            [self getPinNumber];
        }
        else {
            
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"Please check mark T&C."];
        }
    }
    else {
        if (self.txtName.text.length > 0){
        
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"Please enter your Mobile Number."];
        }
        else{
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"Please enter name."];
        }
    }
}


- (IBAction)checkMarkButtonPressed:(id)sender
{
    self.btnCheckMark.selected = !self.btnCheckMark.isSelected;
}


- (IBAction)dateOfBirthButtonPressed:(id)sender
{
    [self dismissKeyboard];
    datePicker.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^
    {
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height - MyDateTimePickerHeight, self.view.frame.size.width, MyDateTimePickerHeight)];
    }];
}

- (IBAction)MaleButtonPressed:(id)sender
{
    [self dismissKeyboard];
    _btnFemale.selected = NO;
    _btnMale.selected = YES;
}

- (IBAction)FeMaleButtonPressed:(id)sender
{
    [self dismissKeyboard];
    _btnFemale.selected = YES;
    _btnMale.selected = NO;
}


- (BOOL)validateMobileNumber:(NSString *)mobileNumber
{
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    BOOL matched = [mobileNumberPred evaluateWithObject:mobileNumber];
    return matched;
}




#pragma mark - TTTAttributedLabelDelegate Methods

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    TermsViewController * termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsController"];
    termsVC.isloggedin = NO;
    [self presentViewController:termsVC animated:YES completion:nil];
}

-(void)setImageOntextField:(UITextField *)textfield andImage:(NSString *)image withSize:(CGSize)size{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, size.width, size.height)];
    imv.image = [UIImage imageNamed:image];
    textfield.rightViewMode = UITextFieldViewModeAlways;
    textfield.rightView = imv;
}


//-(void)DidTakeScreen:(NSNotification *)noti{
//    //
//    //    PHPhotoLibrary *assetLibrary = [[PHPhotoLibrary alloc] init];
//    //    [assetLibrary performChanges:^{
//    //        <#code#>
//    //    } completionHandler:^(BOOL success, NSError * _Nullable error) {
//    //        <#code#>
//    //    }]
//    
//    
//    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
//    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
//    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
//    PHAsset *lastAsset = [fetchResult lastObject];
//    [[PHImageManager defaultManager] requestImageForAsset:lastAsset
//                                               targetSize:CGSizeMake(0, 0)
//                                              contentMode:PHImageContentModeAspectFill
//                                                  options:PHImageRequestOptionsVersionCurrent
//                                            resultHandler:^(UIImage *result, NSDictionary *info) {
//                                                
//                                                NSLog(@"%@",info);
//                                                
//                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                    
//                                                    //                                                    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
//                                                    //                                                        [PHAssetChangeRequest creationRequestForAssetFromImage:[UIImage imageNamed:@"SplashBg"]];
//                                                    //                                                    } completionHandler:^(BOOL success, NSError *error) {
//                                                    //                                                        if (success) {
//                                                    //                                                            NSLog(@"Success");
//                                                    //                                                        }
//                                                    //                                                        else {
//                                                    //                                                            NSLog(@"write error : %@",error);
//                                                    //                                                        }
//                                                    //                                                    }];
//                                                    
//                                                    
//                                                    
//                                                    [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
//                                                        [PHAssetChangeRequest changeRequestForAsset:lastAsset];
//                                                        
//                                                    } completionHandler:^(BOOL success, NSError * _Nullable error) {
//                                                        if (success) {
//                                                            NSLog(@"Success");
//                                                            [PHAssetChangeRequest deleteAssets:[NSArray arrayWithObjects:lastAsset, nil]];
//                                                        }
//                                                        else {
//                                                            NSLog(@"write error : %@",error);
//                                                        }
//                                                    }];
//                                                    
////                                                    [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
////                                                        [PHAssetChangeRequest deleteAssets:[NSArray arrayWithObjects:lastAsset, nil]];
////                                                        
////                                                    } completionHandler:^(BOOL success, NSError * _Nullable error) {
////                                                        if (success) {
////                                                            NSLog(@"Success");
////                                                        }
////                                                        else {
////                                                            NSLog(@"write error : %@",error);
////                                                        }
////                                                    }];
//                                                });
//                                            }];
//}




@end
