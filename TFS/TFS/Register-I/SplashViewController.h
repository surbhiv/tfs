//
//  SplashViewController.h
//  RConnect
//
//  Created by HiteshDhawan on 05/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFS.pch"

@interface SplashViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityController;

@end
