//
//  PinViewController.m
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "PinViewController.h"
#import "TFS.pch"




@implementation PinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Dismiss keyboard gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)pinTextFieldSelected:(id)sender {
    
    _viewPin.layer.borderColor = THEMECOLOR.CGColor;
    _viewPin.layer.borderWidth = 1.5;
    _viewPin.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    _txtPin.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter PIN" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
}


#pragma mark - 
#pragma mark 

- (void)registerUser
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [appDelegate startIndicator];
        NSString *urlString = [NSString stringWithFormat:@"%@user", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, _txtPin.text, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:appDelegate.strUserId, @"UserID", _txtPin.text, @"PIN", [_dicUserInfo objectForKey:@"UserName"], @"Name", nil];
        if ([[_dicUserInfo allKeys] containsObject:@"Gender"]) {
            [paramDic setObject:[_dicUserInfo objectForKey:@"Gender"] forKey:@"Gender"];
        }
        if ([[_dicUserInfo allKeys] containsObject:@"DateOfBirth"]) {
            [paramDic setObject:[_dicUserInfo objectForKey:@"DateOfBirth"] forKey:@"DateOfBirth"];
        }
        
        [paramDic setObject:@" " forKey:@"City"];
        [paramDic setObject:@" " forKey:@"Address"];
    
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request startAsynchronous];
        
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];

    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{

    [appDelegate stopIndicator];
    NSDictionary *dicResponse = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];    
    NSLog(@"response - %@",dicResponse);
    
    if ([[dicResponse allKeys] containsObject:@"UserId"])
    {
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                         kFIRParameterItemID:[NSString stringWithFormat:@"id-Registered"],
                                         kFIRParameterItemName:@"Registered",
                                         kFIRParameterContentType:@""
                                         }];
        
        
        appDelegate.strUserId = [dicResponse objectForKey:@"UserId"];
        
        HomeVC *home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeVC"];
        MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
        
        ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:home];
        naviController.navigationBarHidden = YES;
        
        CGFloat sidemenuWide = SCREEN_WIDTH - 60 ;
        [naviController.sideMenu setMenuWidth:sidemenuWide];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:[_dicUserInfo objectForKey:@"UserName"] forKey:@"UserName"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UserAuthenticated"];
        [[NSUserDefaults standardUserDefaults] setObject:[dicResponse objectForKey:@"UserId"] forKey:@"UserId"];
        [[NSUserDefaults standardUserDefaults] setObject:[_dicUserInfo objectForKey:@"MSN"] forKey:@"MSN"];
        
        if (![[_dicUserInfo objectForKey:@"DateOfBirth"] isEqualToString:@""] && [_dicUserInfo objectForKey:@"DateOfBirth"] != nil) {
            [[NSUserDefaults standardUserDefaults] setObject:[_dicUserInfo objectForKey:@"DateOfBirth"] forKey:@"DateOfBirth"];
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"DateOfBirth"];
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate.justLogedIn = YES;
        
        [appDelegate.window setRootViewController:naviController];
        [appDelegate.window  makeKeyAndVisible];

    }
    else{
        
        if ([dicResponse objectForKey:@"ResponseStatus"] == nil){
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:[@"Server Error" valueForKey:@"Message"]  andButtonTitle:@"Dismiss"];
        }
        else{
			NSDictionary *resp = [dicResponse objectForKey:@"ResponseStatus"];
			
			if ([resp objectForKey:@"ErrorCode"] == 13 || [[resp objectForKey:@"ErrorCode"] isEqualToString:@"13"]){
				[CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"The pin has already expired. Please, try to register again"  andButtonTitle:@"Dismiss"];
			}
			else{
				[CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:[[dicResponse objectForKey:@"ResponseStatus"] valueForKey:@"Message"]  andButtonTitle:@"Dismiss"];
			}
        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription  andButtonTitle:@"Dismiss"];
}

#pragma mark -
#pragma mark Custom Methods

- (void)dismissKeyboard
{
    [self.txtPin resignFirstResponder];
}


- (IBAction)backButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)submitButtonPressed:(id)sender
{
    if (self.txtPin.text.length > 0)
    {
        [self dismissKeyboard];
        [self registerUser];
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"Please enter pin number." andButtonTitle:@"Dismiss"];
    }
}

@end
