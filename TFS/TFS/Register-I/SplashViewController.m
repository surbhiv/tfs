//
//  SplashViewController.m
//  RConnect
//
//  Created by HiteshDhawan on 05/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "SplashViewController.h"


@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    if (status) {
        [self.activityController startAnimating];
    }
    
  
}

-(void)viewWillAppear:(BOOL)animated{
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-Splash"],
                                     kFIRParameterItemName:@"App Lanched",
                                     kFIRParameterContentType:@""
                                     }];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    [UIApplication sharedApplication].statusBarHidden = NO;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}



@end
