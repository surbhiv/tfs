//
//  MenuItemsTVC.h
//  ObjCExample
//
//  Created by Evgeny on 09.01.15.
//  Copyright (c) 2015 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@protocol MenuControllerDelegate <NSObject>

@optional
- (void)menuController:(id)menuController didSelectRow:(NSInteger)row fromMenu:(NSArray *) menu;

@end

@interface MenuItemsTVC : UITableViewController {
    
    NSArray *viewControllers;
    BOOL isOnline;
    UISwitch *statusSwitch;
    
}

@property (nonatomic, assign) id < MenuControllerDelegate > delegate;
@property (nonatomic, strong) NSArray * mainMenuArray;
@property (nonatomic, strong) NSArray * imageArray_mainMenu;

@property int arrCouponCount;
@property int arrRewardsCount;
@property int arrOffersCount;
@property int arrLoyaltyCount;
@property int feedbackCount;
@property int selectedPageOnDetail;

@property NSIndexPath *selectedIndexPath;
@property NSIndexPath *LastselectedIndexPath;

// shared instance
+ (instancetype)sharedInstance;
- (void)showHome;
@end
