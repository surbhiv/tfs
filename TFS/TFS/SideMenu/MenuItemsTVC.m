//
//  MenuItemsTVC.m
//  ObjCExample
//
//  Created by Evgeny on 09.01.15.
//  Copyright (c) 2015 Evgeny Nazarov. All rights reserved.
//

#import "MenuItemsTVC.h"
#import <SVProgressHUD.h>
#import "TFS.pch"


@implementation MenuItemsTVC

+ (instancetype)sharedInstance {
    
    static MenuItemsTVC *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [MenuItemsTVC new];
    });
    return sharedInstance;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    bgView.image = [UIImage imageNamed:@"SplashBg"];
    bgView.contentMode = UIViewContentModeScaleAspectFill;
    
    UIView *backView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [backView addSubview:bgView];
    UIImageView *bgView2 = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    bgView2.backgroundColor = [UIColor colorWithRed:141.0/255.0 green:180.0/255.0 blue:36.0/255.0 alpha:0.70];
    [backView addSubview:bgView2];
    self.tableView.backgroundView = backView;

    self.tableView.allowsSelection = YES;
    
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    _mainMenuArray = [NSArray arrayWithObjects:@"Home",@"Coupons",@"Loyalty Cards",@"Rewards",@"Feedback",@"News",@"My Account",@"Refer a Friend",@"Store Locator",@"Activities",@"Terms & Conditions",@"Contact Us",@"Logout", nil];
    
    _imageArray_mainMenu = [NSArray arrayWithObjects:@"HomeIcon",@"CouponIcon",@"LoyaltyIcon",@"RewardsIcon",@"FeedBackIcon",@"NewsIcon",@"profileIcon",@"referIcon",@"storeIcon",@"activityIcon",@"termsIcon",@"contactIcon",@"logIcon", nil];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}



#pragma mark - TableView
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (IS_IPHONE_X) {
        return 20;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;//3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 50;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _mainMenuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.backgroundColor = [UIColor clearColor];
        UIView *selectionView = [[UIView alloc] initWithFrame:cell.bounds];
        selectionView.backgroundColor = [UIColor colorWithRed: 122.0/255.0 green:159.0/255.0 blue:30.0/255.0 alpha:0.75];//THEMECOLOR;
        [cell setSelectedBackgroundView:selectionView];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    
    CALayer * layer = [CALayer layer];
    layer.frame = CGRectMake (5 ,cell.contentView.frame.size.height - 0.6 ,self.tableView.frame.size.width ,0.6);

    [cell.layer addSublayer:layer];
    
    [cell.textLabel setFont:[UIFont fontWithName:GeorgeRegular size:17.0]];
    cell.textLabel.textColor = [UIColor whiteColor];

        cell.textLabel.text = [NSString stringWithFormat:@" %@",self.mainMenuArray[indexPath.row]];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.imageView.image = [UIImage imageNamed:_imageArray_mainMenu[indexPath.row]];
        UILabel *count = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.size.width - 35, (cell.frame.size.height - 25)/2, 25, 25)];
        count.layer.borderWidth = 1.0;
        count.layer.borderColor = [UIColor blackColor].CGColor;
        count.textColor = [UIColor blackColor];
        count.textAlignment = NSTextAlignmentCenter;
        count.layer.cornerRadius = 12.5;
        count.clipsToBounds = YES;
        count.font = [UIFont fontWithName:GeorgeRegular size:12.5];
        count.tag = 121200;
        
        if ([[cell.contentView subviews] containsObject:[cell.contentView viewWithTag:121200]]) {
            [[cell.contentView viewWithTag:121200] removeFromSuperview];
        }
        
        
        [cell.contentView addSubview:count];

        if (indexPath.row == 1) {
            if (_arrCouponCount > 0) {
                count.hidden = NO;
                count.text = [NSString stringWithFormat:@"%d",_arrCouponCount];
            }
            else{
                count.hidden = YES;
                [count removeFromSuperview];
            }
        }
        else if (indexPath.row == 2)
        {
            if (_arrLoyaltyCount > 0) {
                count.hidden = NO;
                count.text = [NSString stringWithFormat:@"%d",_arrLoyaltyCount];
            }
            else{
                count.hidden = YES;
                [count removeFromSuperview];
            }
        }
        else if (indexPath.row == 3)
        {
            if (_arrRewardsCount > 0) {
                count.hidden = NO;
                count.text = [NSString stringWithFormat:@"%d",_arrRewardsCount];
            }
            else{
                count.hidden = YES;
                [count removeFromSuperview];
            }
        }

        else if (indexPath.row == 4){
            if (_feedbackCount > 0) {
                count.hidden = NO;
                count.text = [NSString stringWithFormat:@"%d",_feedbackCount];
            }
            else{
                count.hidden = YES;
                [count removeFromSuperview];
            }
        }//arrOffersCount
        else if (indexPath.row == 9){
            if (_arrOffersCount > 0) {
                count.hidden = NO;
                count.text = [NSString stringWithFormat:@"%d",_arrOffersCount];
            }
            else{
                count.hidden = YES;
                [count removeFromSuperview];
            }
        }
        else{
            count.hidden = YES;
            [count removeFromSuperview];
        }
    
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(5, cell.textLabel.frame.origin.y + cell.textLabel.frame.size.height, SCREEN_WIDTH - 85, 1)];
    line.backgroundColor = THEMECOLOR;
    line.tag = 909011;

    if ([cell.contentView.subviews containsObject:[cell.contentView viewWithTag:909011]]) {
        [[cell.contentView viewWithTag:909011] removeFromSuperview];
    }
    [cell.contentView addSubview:line];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    if (indexPath.row == 0)
    {
        HomeVC *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        dashboard.title = @"HOME";
        [appDelegate setTopViewwithVC:dashboard];
    }

    else if (indexPath.row == 1)
    {
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"COUPONS";
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;
        NSLog(@"%@",[appDelegate returnTopViewController]);
        
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        
        _arrCouponCount = 0;
    }
    else if (indexPath.row == 2)
    {
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"LOYALTY";
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;

        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }

        _arrLoyaltyCount = 0;
    }
    else if (indexPath.row == 3)
    {
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"REWARDS";
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        _arrRewardsCount = 0;
    }

    else if (indexPath.row == 4)
    {
        FeedbackViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"feedbackController"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 5)
    {
        NewsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"newsController"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 6)
    {
        MoreViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"moreController"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 7)
    {
        ReferViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"referController"];
        dashboard.isShareView = NO;
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 9)
    {
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"OFFERS";
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;
        
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        _arrRewardsCount = 0;
    }
    else if (indexPath.row == 8)
    {
        StoresViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"storesController"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 10)
    {
        TermsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"termsController"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        dashboard.isloggedin = YES;
    }
    else if (indexPath.row == 11){
        
        ContactUsVC *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        
    }
    else if (indexPath.row == 12){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirmation" message:@"Do You Want To Logout ?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD showWithStatus:@"Logging Out..." maskType:SVProgressHUDMaskTypeBlack];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserId"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MSN"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DateOfBirth"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserAuthenticated"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"REGISTERDEVICE"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [appDelegate changeRootViewController];
            [self.navigationController popToRootViewControllerAnimated:YES];

        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                             }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        CFRunLoopWakeUp(CFRunLoopGetCurrent());
    }
    [tableView reloadData];
}

#pragma mark-  Open More Menu Pressed

-(IBAction)OpenMoreMenuPressed:(id)sender
{

}


- (void)showHome {
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

@end
