//
//  constant.h
//  Nepzy
//
//  Created by priya on 06/10/15.
//  Copyright © 2015 priya. All rights reserved.
//

#ifndef constant_h
#define constant_h


//#define BASEURL @"http://liquid-india.feeds.barcodes.no/" // DEMO
#define BASEURL @"http://tfs-india.feeds.barcodes.no/" // LIVE


#define LOCAL_BASEURL @"https://neuronimbusapps.com/24seven/apis/"

//#define SALT @"3g4983hbeioyhqwd78" // DEMO
#define SALT @"71xQQ+)Il6ma_&o6ZsS04Iqs#zLMgWm6Sc&TM_vmQDwoctKU"//LIVE


#define INSTANCE_ID  @"500"

#define mainStoryboard [UIStoryboard storyboardWithName:@"Main" bundle:nil]
#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define THEMECOLOR [UIColor colorWithRed: 122.0/255.0 green:159.0/255.0 blue:30.0/255.0 alpha:1.0]
#define THEMECOLORLight [UIColor colorWithRed:166.0/255.0 green:206.0/255.0 blue:57.0/255.0 alpha:1.0]


#pragma mark - FONT NAMES
#define HelveticaNeueRegular @"HelveticaNeue"
#define HelveticaNeueMedium @"HelveticaNeue-Medium"
#define HelveticaNeueLight @"HelveticaNeue-Light"
#define HelveticaNeueThin @"HelveticaNeue-Thin"
#define HelveticaNeueBold @"HelveticaNeue-Bold"

#define HelveticaRegular @"Helvetica"
#define HelveticaLight @"Helvetica-Light"
#define HelveticaBold @"Helvetica-Bold"

#define GeorgeBold @"GeogrotesqueStencilA-Bd"
#define GeorgeLight @"GeogrotesqueStencilA-Lg"
#define GeorgeMedium @"GeogrotesqueStencilA-Md"
#define GeorgeRegular @"GeogrotesqueStencilA-Rg"
#define GeorgeSemiBold @"GeogrotesqueStencilA-Sb"
#define GeorgeUltaLight @"GeogrotesqueStencilA-Ul"



#define DEVICE_MODEL [[UIDevice currentDevice] platformString]


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)


#define SYS_VERSION_FLOAT (UIDevice.currentDevice().systemVersion as NSString).floatValue
#define iOS7 (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
#define iOS8 (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
#define iOS9 (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
#define iOS10 (iOSVersion.SYS_VERSION_FLOAT >= 10.0 && iOSVersion.SYS_VERSION_FLOAT < 11.0)



#endif /* constant_h */
