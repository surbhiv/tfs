//
//  AppDelegate.m
//  TFS
//
//  Created by Surbhi on 01/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import "AppDelegate.h"
#import "RegisterViewController.h"
#import "TFS-Bridging-Header.h"
#import "TFS-Swift.h"
#import <UserNotifications/UserNotifications.h>
//@synthesize window, imageForR, activityView1;
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface AppDelegate () <CLLocationManagerDelegate, UNUserNotificationCenterDelegate> {
    
    CLLocation *currentLocation;
    UIImageView *imageView;
    
}
@end

@implementation AppDelegate

+ (AppDelegate *) sharedInstance {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [application setApplicationIconBadgeNumber:0];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self createIndicator];

    
    [GMSServices provideAPIKey:@"AIzaSyC_Qb_fuh2oep2SaAP3pJbOi9UgrcoEsoA"]; // Google Map API Key
    [self registerForRemoteNotifications]; // PUSH NOTIFICATION
    [self getUniqueDeviceIdentifierAsString];
    
    [FIRApp configure];
    
    BOOL isUserAuthenticated = [[NSUserDefaults standardUserDefaults] boolForKey:@"UserAuthenticated"];
//    [self getUser];
    
    if (isUserAuthenticated) {
        
        self.strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
        self.strMobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"MSN"];
        
        if ([self.strUserId length] > 0) {
            [self loadFeedbackBacklogVal_AppDelegate];
        }
        else {
            [self getUser]; // Get UserID
        }
        
        HomeVC *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];

        ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:home];
        naviController.navigationBarHidden = YES;

        _isLoader = YES;

        self.isRegistered = [[NSUserDefaults standardUserDefaults]boolForKey:@"REGISTERDEVICE"];
        
        CGFloat sidemenuWide = SCREEN_WIDTH - 75  ;
        [naviController.sideMenu setMenuWidth:sidemenuWide];
        
        [self.window makeKeyAndVisible];
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications]; // geofencing
        [self setUpLocationManagerWithLauncingOptions:launchOptions];
        [self loadStores];
                
        [self.window setRootViewController:naviController];
    
    }
    else {
        
        RegisterViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"registerController"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
        navController.navigationBarHidden = YES;
        self.window.rootViewController = navController;
                _isLoader = NO;
        self.isRegistered = NO;
    }


    
    [self.window makeKeyAndVisible];
    
    [Fabric with:@[[Crashlytics class]]];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}



- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [application setApplicationIconBadgeNumber:0];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}





- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark - UniqueDeviceIdentifier Methods

- (NSString *)getUniqueDeviceIdentifierAsString
{
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"com.neuronimbus.TFS"];
    if (strApplicationUUID == nil)
    {
        strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:appName account:@"com.neuronimbus.TFS"];
    }
    
    NSLog(@"%@--- DEVICE ID",strApplicationUUID);
    
    return strApplicationUUID;
}


#pragma mark -
#pragma mark - NSPushNotification Methods

- (void) registerForRemoteNotifications {
    
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        // Code for old versions
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];

    }
}


//-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//    NSLog(@"User Info : %@",notification.request.content.userInfo);
//    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
//}
//
////Called to let your app know which action was selected by the user for a given notification.
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
//    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
//    completionHandler();
//}

- (void) unregisterForRemoteNotifications {
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    
    if ([self.strDeviceToken isEqualToString:@"randomDeviceTokenNotRegistered"] || self.strDeviceToken != nil || [self.strDeviceToken isEqualToString:@""]){
//        self.strDeviceToken = @"randomDeviceTokenNotRegistered";
    }
    else{
        
    }
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    self.strDeviceToken = [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [Answers logCustomEventWithName:@"DEVICEtokenReceived" customAttributes:@{@"DeviceToken":self.strDeviceToken}];

    if (deviceToken == nil) {
//        self.strDeviceToken = @"randomDeviceTokenNotRegistered";
    }
    else{
        BOOL isUserAuthenticated = [[NSUserDefaults standardUserDefaults] boolForKey:@"UserAuthenticated"];
        
        if (isUserAuthenticated  ) {
            self.strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
            self.strMobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"MSN"];
            
            if ([self.strUserId length] > 0) {
                [self loadFeedbackBacklogVal_AppDelegate];
            }
            else {
                [self getUser]; // Get UserID
            }
            [self performSelector:@selector(registerDevice) withObject:nil afterDelay:1.0];
        }
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    
    [[Crashlytics sharedInstance] recordError:error];
    [Answers logCustomEventWithName:@"FailremoteNotificationRegistration" customAttributes:@{@"Error":error.localizedDescription}];
}


#pragma mark -
#pragma mark - REGISTER DEVICE API

- (void)registerDevice {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {

        if ([self.strDeviceToken isEqualToString:@"randomDeviceTokenNotRegistered"] || [self.strDeviceToken  isEqual: @""] || self.strDeviceToken == nil) {
            // DO Nothing
            [Answers logCustomEventWithName:@"RegisterDevice - No Device Token" customAttributes:@{@"Error":@"No Device Token"}];

        }
        else if (self.strMobileNumber == nil || [self.strMobileNumber isEqualToString: @""]){
            [Answers logCustomEventWithName:@"RegisterDevice - No Mobile Number" customAttributes:@{@"Error":@"Mobile Number NIL"}];
        }
        else if (self.strUserId == nil || [self.strUserId isEqualToString: @""]){
            [Answers logCustomEventWithName:@"RegisterDevice - No User ID" customAttributes:@{@"Error":@"User ID NIL"}];
        }
        else{
            NSString *urlString = [NSString stringWithFormat:@"%@device", BASEURL];
            NSString *_dateString = [HelperClass getDateString];
            [Answers logCustomEventWithName:@"RegisterAPICALLED" customAttributes:@{@"USERID":self.strUserId,@"MOBILE":self.strMobileNumber,@"DeviceToken":self.strDeviceToken}];

            // Generate signature

            NSString *source = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", _dateString, [[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""], self.strUserId, self.strDeviceToken, self.strMobileNumber, @"en", @"iOS", [[UIDevice currentDevice] systemVersion], SALT];
            NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
            
            ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
            [request setTimeOutSeconds:30.0];
            [request setDelegate:self];
            
            [request addRequestHeader:@"Accept" value:@"application/json"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
            [request addRequestHeader:@"X-Liquid-Signature" value:signature];
            
            NSString *deviceID = [[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
            if ([deviceID isEqualToString:@""] || deviceID ==  nil){
                deviceID =  @"";
            }
            
            NSDictionary *paramDic = @{@"UserID":[NSString stringWithFormat:@"%@",self.strUserId],
                                       @"DeviceId":[[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""],
                                       @"DeviceToken":[NSString stringWithFormat:@"%@",self.strDeviceToken],
                                       @"MSN":[NSString stringWithFormat:@"%@",self.strMobileNumber],
                                       @"Language":@"en",
                                       @"DeviceType":@"iOS",
                                       @"AppVersion":[[UIDevice currentDevice] systemVersion]
                                       };
            
            NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
            [request setPostBody:[NSMutableData dataWithData:data]];
            [request setDidFinishSelector:@selector(requestFinished_Register:)];
            [request setDidFailSelector:@selector(requestFailed_Register:)];
            
            [request startAsynchronous];

        }
    }
//    else {
//
//        NSLog(@"Error: There is no network, please check your internet.");
//    }
}

- (void)requestFinished_Register:(ASIHTTPRequest *)request
{
    [Answers logCustomEventWithName:@"RegisterAPISucessRequest" customAttributes:@{@"USERID":self.strUserId,@"MOBILE":self.strMobileNumber,@"DeviceToken":self.strDeviceToken}];

    NSDictionary *dicFeedback = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];

    if ([[dicFeedback allKeys] containsObject:@"Backlog"]) {
        _backlogVal = [[dicFeedback valueForKey:@"Backlog"] intValue];
    }
    else {
        if (![self.strDeviceToken isEqualToString:@"randomDeviceTokenNotRegistered"] && ![self.strDeviceToken  isEqual: @""] && self.strDeviceToken != nil) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"REGISTERDEVICE"];
            self.isLoader = YES;
            self.isRegistered = YES;
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (void)requestFailed_Register:(ASIHTTPRequest *)request
{
    [[Crashlytics sharedInstance] recordError:request.error];
    [Answers logCustomEventWithName:@"DEVICEREGISTERAPIFailed" customAttributes:@{@"Error":[request.error localizedDescription],@"USERID":self.strUserId,@"MOBILE":self.strMobileNumber,@"DeviceToken":self.strDeviceToken}];

}



#pragma mark -
#pragma mark - CLLocationManagerDelegate Methods

- (void) setUpLocationManagerWithLauncingOptions: (NSDictionary *)launchOptions {
    
    // Initialize Location Manager
    self.locationManager = [[CLLocationManager alloc] init];
    
    // Configure Location Manager
    [self.locationManager setDelegate:self];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    _locationManager.distanceFilter=kCLDistanceFilterNone;
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager startMonitoringSignificantLocationChanges];
    
    // Load Geofences
    self.geofences = [NSMutableArray arrayWithArray:[[self.locationManager monitoredRegions] allObjects]];
    self.strUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
    [self.locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    currentLocation = [locations lastObject];
    for (CLCircularRegion *circle in self.latestGeofences) {
        if (![self.geofences containsObject:circle]) {
            [self.locationManager startMonitoringForRegion:circle]; // Start Monitoring Region
        }
    }
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
}



- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    //    [self pushCoupon];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"Welcome!" andMessage:[NSString stringWithFormat:@"you are nearby - %@", region.identifier]];
    }
    else{
        
        UILocalNotification * notification = [[UILocalNotification alloc] init];
        notification.alertBody = [NSString stringWithFormat:@"Welcome! : you are nearby - %@", region.identifier];
        notification.soundName = @"Default";
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"EXIT" andMessage:[NSString stringWithFormat:@"you are now exit from - %@", region.identifier]];
    }
    else{
        
        UILocalNotification * notification = [[UILocalNotification alloc] init];
        notification.alertBody = [NSString stringWithFormat:@"Thank You! : you are now exit from - %@", region.identifier];
        notification.soundName = @"Default";
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    }

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    
}

+ (NSString *)kilometersFromPlace:(CLLocationCoordinate2D)from andToPlace:(CLLocationCoordinate2D)to {
    
    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:from.latitude longitude:from.longitude];
    CLLocation *storeLoc = [[CLLocation alloc] initWithLatitude:to.latitude longitude:to.longitude];
    CLLocationDistance distance = [userLoc distanceFromLocation:storeLoc]/1000;
    return [NSString stringWithFormat:@"%1f", distance];
}

#pragma mark
#pragma mark - User Details
- (void)getUser {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus reachStatus = [reachability currentReachabilityStatus];
    
    if (reachStatus) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@app", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, INSTANCE_ID, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        NSLog(@"GET USER ID API");
        NSLog(@"CURRENT DATE --- %@",_dateString);
        NSLog(@"INSTANCE ID --- %@",INSTANCE_ID);
        NSLog(@"SALT --- %@",SALT);
        NSLog(@"SOURCE --- %@",source);
        NSLog(@"SIGNATURE --- %@",signature);
        
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        
        [request setTimeOutSeconds:30000];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"InstanceId":INSTANCE_ID, @"UDID":[[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""]};
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        NSLog(@"\n\n\n\n DEVICE ID ---- %@\n\n\n",[[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""]);

        [request setDidFinishSelector:@selector(getUserRequestSuccessful:)];
        [request setDidFailSelector:@selector(getUserRequestFailed:)];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else {
        
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"Error!" andMessage:@"Please check your internet connection." andButtonTitle:@"Ok"];
    }
    
}

- (void)getUserRequestSuccessful:(ASIHTTPRequest *)request {
    
    NSDictionary *responseDic = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:nil]];
    NSLog(@"USER REQUEST SUCCESS");
    NSLog(@"response -- %@",responseDic);
    if ([[responseDic allKeys] containsObject:@"UserId"]) {
        
        self.strUserId = [responseDic objectForKey:@"UserId"];
        NSLog(@"USER ID ---- %@",self.strUserId);

        [[NSUserDefaults standardUserDefaults] setObject:[responseDic objectForKey:@"UserId"] forKey:@"UserId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"USERIDCREATED" object:nil userInfo:nil];
    }
}

- (void)getUserRequestFailed:(ASIHTTPRequest *)request {
    
    NSLog(@"USER REQUEST FAILED");

//      NSDictionary *responseDic = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
}

#pragma mark
#pragma mark - SIDE MENU METHODS

-(void)setTopViewwithVC:(id)viewControl// Done By Surbhi
{
        ENSideMenuNavigationController *naviController = (ENSideMenuNavigationController *)[self.window rootViewController];
        if (![[naviController topViewController] isKindOfClass:[viewControl class]]) {
            naviController.navigationBarHidden = YES;
            [naviController setContentViewController:viewControl];
            CFRunLoopWakeUp(CFRunLoopGetCurrent());
        }
        [naviController hideSideMenuView];
}

-(id)returnTopViewController{
    ENSideMenuNavigationController *naviController = (ENSideMenuNavigationController *)[self.window rootViewController];
    return  [naviController topViewController];
}


- (void)changeRootViewController {
    
    RegisterViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"registerController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
    navController.navigationBarHidden = NO;
    _isLoader = NO;
    [SVProgressHUD dismiss];

    self.window.rootViewController = navController;
}

#pragma mark -
#pragma mark - UIBarButtoItem Methods
- (void)loadFeedbackBacklogVal_AppDelegate {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *_catString = @"SHOPEXPERIENCE";
        NSString *urlString = [NSString stringWithFormat:@"%@RatingState/%@/%@", BASEURL, _strUserId, _catString];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, self.strUserId, _catString, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinished_AppDelegate:)];
        [request setDidFailSelector:@selector(requestFailed_AppDelegate:)];

        [request startAsynchronous];
    }
    else {
        
        [self stopIndicator];
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}
- (void)requestFinished_AppDelegate:(ASIHTTPRequest *)request
{
    NSDictionary *dicFeedback = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [self stopIndicator];

    if ([[dicFeedback allKeys] containsObject:@"Backlog"]) {
        
        _backlogVal = [[dicFeedback valueForKey:@"Backlog"] intValue];
    }
    else  {

    }
}

- (void)requestFailed_AppDelegate:(ASIHTTPRequest *)request
{
    [self stopIndicator];
}

#pragma mark - LoadFeedback from Home

- (void)loadFeedbackBacklogValue {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *_catString = @"SHOPEXPERIENCE";
        NSString *urlString = [NSString stringWithFormat:@"%@RatingState/%@/%@", BASEURL, _strUserId, _catString];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, self.strUserId, _catString, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];

        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [self stopIndicator];
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSDictionary *dicFeedback = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];

    if ([[dicFeedback allKeys] containsObject:@"Backlog"]) {
        
        _backlogVal = [[dicFeedback valueForKey:@"Backlog"] intValue];
    }
    else if ([[dicFeedback objectForKey:@"http_code"] integerValue] == 200) {

    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"BACKLOGUpdated" object:nil];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"BACKLOGUpdated" object:nil];
}

#pragma mark -
#pragma mark - ASIHTTPRequest Methods

- (void)loadStores {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@Stores/%@", BASEURL, self.strUserId];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, self.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinishedd:)];
        [request setDidFailSelector:@selector(requestFailedd:)];
        [request startAsynchronous];
    }
}

- (void)requestFinishedd:(ASIHTTPRequest *)request {
    
    NSDictionary *dicStores = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicStores allKeys] containsObject:@"Stores"]) {
        
        NSMutableArray *storeArray = [[NSMutableArray alloc] init];
        
        if (currentLocation != nil) {
            
            for (NSDictionary *dic in [dicStores objectForKey:@"Stores"]) {
                
                @autoreleasepool {
                    
                    NSMutableDictionary *dicObj = [[NSMutableDictionary alloc] initWithDictionary:dic];
                    
                    CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:[[dicObj objectForKey:@"Latitude"] floatValue] longitude:[[dicObj objectForKey:@"Longitude"] floatValue]];
                    [dicObj setObject:[AppDelegate kilometersFromPlace:currentLocation.coordinate andToPlace:storeLocation.coordinate] forKey:@"Distance"];
                    
                    // geofencing
                    CLCircularRegion *circle = [[CLCircularRegion alloc] initWithCenter:[storeLocation coordinate] radius:100.0 identifier:dicObj[@"Name"]];
                    
                    circle.notifyOnEntry = YES;
                    circle.notifyOnExit = YES;
                    
                    if (self.latestGeofences == nil) {
                        self.latestGeofences = [NSMutableArray array];
                    }
                    [self.latestGeofences addObject:circle];
                    
                    
                    NSMutableArray *openArr = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"OpeningHours"]];;
                    for (int i = 0; i < openArr.count; i++) {
                        
                        if (openArr[i]  == [NSNull null]) {
                            [openArr replaceObjectAtIndex:i withObject:@""];
                        }
                    }
                    [dicObj setObject:openArr forKey:@"OpeningHours"];

                    
                    
                    [storeArray addObject:dicObj];
                }
            }
            
            [self.locationManager startUpdatingLocation];
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Distance" ascending:YES];
            self.arrStores = [[NSArray alloc] initWithArray:[storeArray mutableCopy]];
            self.arrStores = [self.arrStores sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            
            //
            // Save data to prefrences
            [[NSUserDefaults standardUserDefaults] setObject:self.arrStores forKey:@"Stores"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else {
            
            self.arrStores = [[NSArray alloc] initWithArray:[dicStores objectForKey:@"Stores"]];
        }
    }
    
}

- (void)requestFailedd:(ASIHTTPRequest *)request
{
//    NSLog(@"error - %@", [request.error localizedDescription]);
}

#pragma mark - Loader
//MARK:- INDICATOR VIEW
-(void)createIndicator
{
    self.activityView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.activityView1.backgroundColor = [UIColor clearColor];
    
    UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    back.backgroundColor =[UIColor blackColor];
    back.alpha = 0.5;
    [self.activityView1 addSubview:back];
    
    self.logoView = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 75)/2, (SCREEN_HEIGHT - 75)/2, 75, 75)];
    self.logoView.layer.cornerRadius = 37.5;
    self.logoView.backgroundColor = [UIColor colorWithRed:166.0/255.0 green:206.0/255.0 blue:57.0/255.0 alpha:1.0];
    
    self.imageForR = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, 45, 45)];
    self.imageForR.image = [UIImage imageNamed:@"marker"];
    self.imageForR.backgroundColor = [UIColor clearColor];
    self.imageForR.contentMode = UIViewContentModeScaleAspectFit;
    
    
    self.imageText = [[UIImageView alloc] initWithFrame:CGRectMake(17.5, 55, 40, 11)];
    self.imageText.image = [UIImage imageNamed:@"text.png"];
    self.imageText.backgroundColor = [UIColor clearColor];
    self.imageText.contentMode = UIViewContentModeScaleAspectFit;

    
    [self.logoView addSubview:self.imageForR];
    [self.logoView addSubview:self.imageText];
    
    [self.activityView1 addSubview:self.logoView];
    
    [self.window addSubview:self.activityView1];
    [self.window sendSubviewToBack:self.activityView1];
}

-(void)startIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.window addSubview:self.activityView1];
        [self rotate360Degrees];
        [self.window bringSubviewToFront:self.activityView1];

    });
}

-(void)stopIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageForR.layer  removeAllAnimations];
        [self.window willRemoveSubview:self.activityView1];
        [self.activityView1 removeFromSuperview];
     });
}


-(void)rotate360Degrees
{
    CABasicAnimation *rotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotate.fromValue = @(0);
    rotate.toValue = @(180);
    rotate.duration = 50;
    rotate.repeatCount = 100.0;
    [self.imageForR.layer addAnimation:rotate forKey:@"myRotationAnimation"];
}


-(void)showWelcomePop:(NSString *)name{
    
    self.activityView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.activityView2.backgroundColor = [UIColor clearColor];
    
    UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    back.backgroundColor =[UIColor blackColor];
    back.alpha = 0.5;
    [self.activityView2 addSubview:back];
    
    UIView *popBox = [[UIView alloc] initWithFrame:CGRectMake(35, (SCREEN_HEIGHT - 150)/2, SCREEN_WIDTH - 70, 150)];
    popBox.backgroundColor = [UIColor whiteColor];
    popBox.layer.cornerRadius = 8.0;

    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((popBox.frame.size.width - 45)/ 2, 10, 45, 45)];
    logo.image = [UIImage imageNamed:@"logoBig"];
    logo.backgroundColor = [UIColor clearColor];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [popBox addSubview:logo];

    UILabel *welcomeLbl = [[UILabel alloc] initWithFrame:CGRectMake(25, 60, SCREEN_WIDTH - 120, 50)];
    welcomeLbl.numberOfLines = 0;
    welcomeLbl.font = [UIFont fontWithName:GeorgeSemiBold size:14.5];
    welcomeLbl.textColor = [UIColor blackColor];
    welcomeLbl.textAlignment = NSTextAlignmentCenter;
    welcomeLbl.text = [NSString stringWithFormat:@"Hi %@,\nWelcome",name];// to TFS India
    [popBox addSubview:welcomeLbl];
    
    UIButton *okBtn = [[UIButton alloc] initWithFrame:CGRectMake((popBox.frame.size.width - 80), 115, 70, 30)];
    [okBtn setTitle:@"OK" forState:UIControlStateNormal];
    [okBtn setTitleColor:THEMECOLOR forState:UIControlStateNormal];
    okBtn.titleLabel.font = [UIFont fontWithName:GeorgeSemiBold size:16];
    [okBtn addTarget:self action:@selector(hideWelcomePop) forControlEvents:UIControlEventTouchUpInside];
    [popBox addSubview:okBtn];

    [self.activityView2 addSubview:popBox];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.window addSubview:self.activityView2];
        [self.window bringSubviewToFront:self.activityView2];
    });
}

-(void)hideWelcomePop{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.window willRemoveSubview:self.activityView2];
        [self.activityView2 removeFromSuperview];
    });
}

@end
