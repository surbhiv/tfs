//
//  ReferViewController.h
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import <MessageUI/MessageUI.h>
#import <ContactsUI/ContactsUI.h>
#import "ContactViewController.h"

@interface ReferViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, MFMessageComposeViewControllerDelegate, CNContactViewControllerDelegate, CNContactPickerDelegate>

@property (nonatomic, strong) NSArray *arrCountryCode;
@property (strong, nonatomic) CNContactPickerViewController *contactPicker;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;


@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneCode;
@property (weak, nonatomic) IBOutlet UIView *viewmobileu;

- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)sendButtonPressed:(id)sender;
- (IBAction)contactsButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *referView;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property BOOL isShareView;
@property NSString *campeignID;
@property NSString *couponID;


@property (weak, nonatomic) IBOutlet UITextField *txtphoneCode_Share;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile_Share;
@property (weak, nonatomic) IBOutlet UIButton *contactButton_Share;
@property (weak, nonatomic) IBOutlet UIView *viewmobileu_Share;

- (IBAction)ShareButtonButtonPressed:(id)sender;
- (IBAction)CancelSharePressed:(id)sender;



@end
