//
//  FeedbackViewController.m
//  RConnect
//
//  Created by HiteshDhawan on 27/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "FeedbackViewController.h"
#import "TFS.pch"

@interface FeedbackViewController ()
{
    NSInteger ratingIndex;
    CGFloat ratingValue;

    UIActivityIndicatorView *refreshActivityView;
    NSTimer * timer;
    int miliSecond;
    ASIHTTPRequest *requestFeedback;
    
}

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    self.contentView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.shadowOpacity = 1;
    self.contentView.layer.shadowOffset = CGSizeZero;
    self.contentView.layer.shadowRadius = 2;
    self.contentView.layer.cornerRadius = 3.0;
    
    self.contentView.layer.masksToBounds = NO;
    
    self.navigationController.navigationBar.tintColor = THEMECOLOR;
}

- (IBAction)toggleSideMenuView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

//    [self.navigationController toggleSideMenuView];
}



- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    // Load feedback
    [appDelegate startIndicator];
    [self loadFeedback];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [self pauseTimer];
    
    // Cancel ASIHTTPRequest
    if (![requestFeedback isCancelled]) {
        
        [requestFeedback cancel];
        [requestFeedback clearDelegatesAndCancel];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


#pragma mark -
#pragma mark UIBarButtoItem Methods

- (void)loadFeedback {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *_catString = @"SHOPEXPERIENCE";
        NSString *urlString = [NSString stringWithFormat:@"%@RatingState/%@/%@", BASEURL, appDelegate.strUserId, _catString];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, _catString, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];

    }
}

- (void)rateProduct {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [SVProgressHUD showWithStatus:@"Submit Rating..." maskType:SVProgressHUDMaskTypeGradient];
        
        NSString *urlString = [NSString stringWithFormat:@"%@RateProduct", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        NSString *appId = @"12345";
        NSString *ratingId = [_dicFeedback valueForKey:@"RatingId"];
        NSString *shopId = [_dicFeedback valueForKey:@"ShopId"];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@%d%@%@", _dateString, appDelegate.strUserId, appId, ratingId, (int)ratingValue, shopId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"UserID":appDelegate.strUserId,
                                   @"AppId":appId,
                                   @"RatingId":ratingId,
                                   @"Rating":[NSString stringWithFormat:@"%d", (int)ratingValue],
                                   @"ShopId":shopId};
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request startAsynchronous];
        
    }
    else {
        
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    [appDelegate stopIndicator];
    
    self.dicFeedback = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [self pauseTimer];
    
    if ([self.delegate respondsToSelector:@selector(setBadgeValueForButtonThree:)]) {
        [self.delegate setBadgeValueForButtonThree:[[_dicFeedback valueForKey:@"Backlog"] intValue]];
    }
    
    if ([[_dicFeedback allKeys] containsObject:@"Bonus"]) {
        
        [self pauseTimer];
        [self refreshButtonPressed:nil];
    }
    if ([[_dicFeedback valueForKey:@"Backlog"] integerValue] > 0) {
        
        NSMutableAttributedString *attributedString = [self shoppingExperience:[NSString stringWithFormat:@"\n %@", [_dicFeedback valueForKey:@"ShopName"]] andTime:nil];
        self.lblStoreName.attributedText = attributedString;
        self.lblShoppingExp.text = @"";
        
        self.lblRateEnds.text = [NSString stringWithFormat:@"Opportunity to rate ends in %@", [self timeIntervaltoDate:[_dicFeedback valueForKey:@"TimeToLive"]]];
        
        int ratingVal = [[_dicFeedback valueForKey:@"NoRatings"] intValue]%5;
        self.lblStatus.text = [NSString stringWithFormat:@"Status: %d out of 5 ratings", ratingVal];
        self.lblRateMore.text = [NSString stringWithFormat:@"You need to rate %ld more times", (long)5 - ratingVal];
        self.lblTotalRate.text = [NSString stringWithFormat:@"Total number of ratings: %d", [[_dicFeedback valueForKey:@"NoRatings"] intValue]];
        
        if ([[_dicFeedback valueForKey:@"NoRatings"] integerValue] > 0) {
            
            for (int i=200; i<=204; i++) {
                UIImageView *imageView = (UIImageView *)[self.view viewWithTag:i];
                if (i<ratingVal+200) imageView.image = [UIImage imageNamed:@"smile"];
                else imageView.image = [UIImage imageNamed:@"smile_grey"];
            }
        }
        
        [self startTimer];
        
        // Start countdown
        [self timeIntervaltoDate:[_dicFeedback objectForKey:@"TimeToLive"]];
    }
    else {
        
        appDelegate.backlogVal = [[_dicFeedback valueForKey:@"Backlog"] intValue];
        
        self.lblShoppingExp.text = @"From time to time we will ask you to rate your shopping experience. After 5 ratings we will reward you with a surprise coupon.";
        
        int ratingVal = [[_dicFeedback valueForKey:@"NoRatings"] intValue]%5;
        self.lblStatus.text = [NSString stringWithFormat:@"Status: %d out of 5 ratings", ratingVal];
        self.lblRateMore.text = [NSString stringWithFormat:@"You need to rate %ld more times", (long)5 - ratingVal];
        self.lblTotalRate.text = [NSString stringWithFormat:@"Total number of ratings: %d", [[_dicFeedback valueForKey:@"NoRatings"] intValue]];
        self.lblRateEnds.text = @"";
        self.lblStoreName.text = @"";
        
        for (int i=100; i<=104; i++) {
            UIButton *btnRating = (UIButton *)[self.view viewWithTag:i];
            [btnRating setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        }
        
        for (int i=200; i<=204; i++) {
            UIImageView *imageView = (UIImageView *)[self.view viewWithTag:i];
            if (i<ratingVal+200) imageView.image = [UIImage imageNamed:@"smile"];
            else imageView.image = [UIImage imageNamed:@"smile_grey"];
        }
        
        ratingValue = 0.0;
        [self pauseTimer];
    }
    [self UpdateSideMenuTable];
}


-(void)UpdateSideMenuTable{
    
    NSInteger numberOfCoupons = [[NSUserDefaults standardUserDefaults] integerForKey:@"COUPONCOUNT"];
    NSInteger numberOfLoaylty = [[NSUserDefaults standardUserDefaults] integerForKey:@"LOYALTYCOUNT"];
    NSInteger numberOfRewards = [[NSUserDefaults standardUserDefaults] integerForKey:@"REWARDSCOUNT"];
    NSInteger numberOfOffers = [[NSUserDefaults standardUserDefaults] integerForKey:@"OFFERSCOUNT"];

    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
    mo.arrCouponCount = (int)[[appDelegate.dicOffers objectForKey:@"Coupons"] count] - (int)numberOfCoupons;
    mo.arrLoyaltyCount = (int)[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] - (int)numberOfLoaylty;
    mo.arrRewardsCount = (int)[[appDelegate.dicOffers objectForKey:@"Rewards"] count] - (int)numberOfRewards;
    mo.arrOffersCount = (int)[[appDelegate.dicOffers objectForKey:@"Offers"] count] - (int)numberOfOffers;

    mo.feedbackCount = appDelegate.backlogVal;
    
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] forKey:@"LOYALTYCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Rewards"] count] forKey:@"REWARDSCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Coupons"] count] forKey:@"COUPONCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Offers"] count] forKey:@"OFFERSCOUNT"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    [mo.tableView reloadData];
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    _btnRefresh.hidden = NO;
    [refreshActivityView removeFromSuperview];
    [appDelegate stopIndicator];

    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];
}

#pragma mark -
#pragma mark UIBarButtoItem Methods

- (IBAction)refreshButtonPressed:(id)sender {
    
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    
    _btnRefresh.hidden = YES;
    [self.headerView addSubview:refreshActivityView];
    
    for (int i=100; i<=104; i++) {
        UIButton *btnRate = (UIButton *)[self.view viewWithTag:i];
        [btnRate setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
    }
    
    [self loadFeedback];
}


- (IBAction)submitRateButtonPressed:(id)sender {
    
    if ([[_dicFeedback valueForKey:@"Backlog"] integerValue] > 0 && ratingIndex > 0) {
        [self rateProduct];
    }
}

- (IBAction)starRatingButtonPressed:(id)sender {
    
    if ([[_dicFeedback valueForKey:@"Backlog"] integerValue] > 0) {
        
        
        NSInteger tagValue = ((UIButton *)sender).tag;
        ratingValue = 0.0;
        
        for (int i=100; i<=104; i++) {
            
            UIButton *btnRate = [self.view viewWithTag:i];
            if (i<=tagValue) {
                if (ratingIndex == i && ratingIndex == tagValue) {
                    
                    [btnRate setImage:[UIImage imageNamed:@"star_half"] forState:UIControlStateNormal];
                    ratingValue = ratingValue + .5;
                } else {
                    ratingValue = ratingValue + 1.0;
                    [btnRate setImage:[UIImage imageNamed:@"star_green"] forState:UIControlStateNormal];
                }
            }
            else {
                [btnRate setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
            }
        }
        
        ratingIndex = tagValue;
    }
}


- (NSString *)timeIntervaltoDate:(NSString*)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate *startDate = [NSDate date];
    
    NSDate *endDate = [f dateFromString:date];
    
    NSCalendar* currentCalendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *differenceComponents = [currentCalendar components:unitFlags fromDate:startDate toDate:endDate options:0];
    
//    NSInteger yearDifference = [differenceComponents year];
//    NSInteger monthDifference = [differenceComponents month];
    NSInteger dayDifference = [differenceComponents day];
    NSInteger hourDifference = [differenceComponents hour];
    NSInteger minuteDifference = [differenceComponents minute];
    NSInteger secondDifference = [differenceComponents second];
    
    int miliHours = (int)(((hourDifference * 60) * 60) * 1000);
    int miliMinuts = (int)((minuteDifference * 60) * 1000);
    int secondMili = (int)secondDifference * 1000;
    
    miliSecond = miliHours + miliMinuts + secondMili;
    
    if (dayDifference > 0) {
        return [NSString stringWithFormat:@"%02ld days",(long)dayDifference];
    }
    else {
        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)hourDifference, (long)minuteDifference, (long)secondDifference];
    }
    
    return nil;
}


- (NSMutableAttributedString *)shoppingExperience:(NSString *)nameString andTime:(NSString *)timeString {
    
    NSString *shoppingString = [NSString stringWithFormat:@"Thank you for your visit at %@.", nameString];
    NSRange nameRange = [shoppingString rangeOfString:nameString];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:shoppingString];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0] range:nameRange];
    
    return attributedString;
}


#pragma mark -
#pragma mark NSTimer Methods

- (void)updateTimer:(NSTimer *)timer {
    miliSecond -= 1000 ;
    [self populateLabelwithTime:miliSecond];
}

- (void)startTimer {
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
}

-(void)pauseTimer {
    if ([timer isValid]) {
        [timer invalidate];
    }
}

- (void)populateLabelwithTime:(int)milliseconds {
    
    int seconds = milliseconds/1000;
    int minutes = seconds / 60;
    int hours = minutes / 60;
    
    if (seconds == 0 && minutes == 0 && hours == 0) {
        [self pauseTimer];
    }
    else {
        seconds -= minutes * 60;
        minutes -= hours * 60;
        
        NSString *remainingTime = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
        
        NSMutableAttributedString *attributedString = [self shoppingExperience:[NSString stringWithFormat:@"\n %@", [_dicFeedback valueForKey:@"ShopName"]] andTime:nil];
        self.lblStoreName.attributedText = attributedString;
        
        self.lblRateEnds.text = [NSString stringWithFormat:@"Opportunity to rate ends in %@", remainingTime];
    }
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//        [mo showHome];
        [self dismissViewControllerAnimated:true completion:nil];
    }
}
@end
