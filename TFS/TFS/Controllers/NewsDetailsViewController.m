//
//  NewsDetailsViewController.m
//  RConnect
//
//  Created by HiteshDhawan on 31/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "NewsDetailsViewController.h"
#import "TFS.pch"

//#import "KioskWebViewController.h"



@interface NewsDetailsViewController ()

@end

@implementation NewsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _contentView.layer.cornerRadius = 6.0;
    _contentView.clipsToBounds = YES;
    _contentView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _contentView.layer.shadowOpacity = 1;
    _contentView.layer.shadowOffset = CGSizeZero;
    _contentView.layer.shadowRadius = 3.5;
    _contentView.layer.masksToBounds = NO;

    
    [_contentView layoutIfNeeded];
    [_imgNewsImage layoutIfNeeded];
    
    [self.imgNewsImage sd_setImageWithURL:[NSURL URLWithString:[self.dicNews objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    
    NSString *titleString = [self.dicNews objectForKey:@"title"];
    if (![titleString isEqual:[NSNull null]]) {
        [self.lblTitle setText:titleString];
    }
    
    NSString *publishDate = [self.dicNews objectForKey:@"publish_date"];
    if (![publishDate isEqual:[NSNull null]]) {
        [self.lblPublishDate setText:publishDate];
        self.lblPublishDate.textAlignment = NSTextAlignmentLeft;
    }
    
    NSString *htmlString = [self.dicNews objectForKey:@"long_description"];
    if (![htmlString isEqual:[NSNull null]]) {
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont systemFontOfSize:16.0]
                                 range:NSMakeRange(0, attributedString.length)];
        
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.alignment = NSTextAlignmentLeft;//NSTextAlignmentJustified;
        paragraphStyle.paragraphSpacing = 10.0;
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
        _txtDescription.attributedText = attributedString;
    }
    
    
//    CGSize sizeThatFitsTextView = [_txtDescription sizeThatFits:CGSizeMake(_txtDescription.frame.size.width, MAXFLOAT)];
}

-(void)viewWillAppear:(BOOL)animated{
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"NEWS DETAIL"],
                                     kFIRParameterItemName:@"NEWS DETAIL",
                                     kFIRParameterContentType:@""
                                     }];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


- (IBAction)backButtonPressed:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (NSString *)convertStringToDate:(NSString *)dateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *newsDate = [formatter dateFromString:dateString];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [formatter stringFromDate:newsDate];
    
    return date;
}

- (NSString *)convetDateToString:(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

#pragma mark - 
#pragma mark UITextViewDelegate Methods

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    
    KioskWebViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webController"];
    viewController.urlString = [URL relativeString];
    [self presentViewController:viewController animated:YES completion:nil];
//
    return NO;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//        [mo showHome];
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        mo.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}
@end
