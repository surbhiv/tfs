//
//  DetailsViewController.m
//  TwentyFourSeven
//
//  Created by Hitesh Dhawan on 06/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "CouponDetailsViewController.h"
#import "TFS.pch"
#import <Photos/Photos.h>

@interface CouponDetailsViewController () <UIPopoverPresentationControllerDelegate>
{
    UIScrollView *_scrollView;
    UIActivityIndicatorView *refreshActivityView;
    UILabel *noContentLabel;
}
@end

@implementation CouponDetailsViewController 

- (void)viewDidLoad {
    
    [appDelegate startIndicator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationReceived:) name:@"SharingCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GoHome:) name:@"OfferCancelled" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DidTakeScreen:) name:UIApplicationUserDidTakeScreenshotNotification object:nil];

    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = THEMECOLOR;
    
    if (_scrollView != nil) {
        [_scrollView removeFromSuperview];
        _scrollView = nil;
    }
    
    if (([appDelegate.dicOffers objectForKey:@"Coupons"] == nil && [self.title isEqualToString:@"COUPONS"]) ||
        ([appDelegate.dicOffers objectForKey:@"Loyalty"] == nil && [self.title isEqualToString:@"LOYALTY"]) ||
        ([appDelegate.dicOffers objectForKey:@"Rewards"] == nil && [self.title isEqualToString:@"REWARDS"]) ||
        ([appDelegate.dicOffers objectForKey:@"Offers"] == nil && [self.title isEqualToString:@"OFFERS"]) )  {

        [self updateOffers];
    }
    else{
        if ([self.title isEqualToString:@"COUPONS"]) {
            
            self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Coupons"]];
        }
        else if ([self.title isEqualToString:@"LOYALTY"]) {
            
            self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Loyalty"]];
        }
        else if ([self.title isEqualToString:@"REWARDS"]){
            
            self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Rewards"]];
        }
        else if ([self.title isEqualToString:@"OFFERS"]){
            
            self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Offers"]];
        }
        
        self.initialIndex = self.selectedPageOnDetail;
        
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [self showLodingIndicator:NO];
        [appDelegate stopIndicator];
        [self updateCustomPageViewController];

    }
    
    
    
    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
    mo.selectedPageOnDetail = 0;
}

-(void)NotificationReceived:(NSNotification *)noti{
    if (_scrollView != nil) {
        [_scrollView removeFromSuperview];
        _scrollView = nil;
    }
    [appDelegate startIndicator];
    [self updateOffers];
}



-(void)viewWillAppear:(BOOL)animated{
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",self.title],
                                     kFIRParameterItemName:self.title,
                                     kFIRParameterContentType:@""
                                     }];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}






#pragma mark -
#pragma mark API Methods

- (void)updateOffers {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [self showLodingIndicator:YES];
        
        NSString *urlString = [NSString stringWithFormat:@"%@coupons/%@/true", BASEURL, appDelegate.strUserId];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [appDelegate stopIndicator];

        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    [self showLodingIndicator:NO];
    [appDelegate stopIndicator];
    
    appDelegate.dicOffers = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([self.title isEqualToString:@"COUPONS"]) {
        
        self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Coupons"]];
    }
    else if ([self.title isEqualToString:@"LOYALTY"]) {
        
        self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Loyalty"]];
    }
    else if ([self.title isEqualToString:@"REWARDS"]){
        
        self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Rewards"]];
    }
    else if ([self.title isEqualToString:@"OFFERS"]){
        
        self.arrContent = [NSArray arrayWithArray:[appDelegate.dicOffers objectForKey:@"Offers"]];
    }
    
    
    
    if (self.arrContent.count > 0) {
        [self updateCustomPageViewController];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    [self showLodingIndicator:NO];
    [appDelegate stopIndicator];
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];

}

#pragma mark - SHARE BUTTON
- (IBAction)shareButtonPressed:(id)sender {

    NSInteger index = self.pageControl.currentPage;
    ReferViewController *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"referController"];
    dashboard.isShareView = YES;
    dashboard.couponID = [NSString stringWithFormat:@"%@",[[_arrContent objectAtIndex:index]valueForKey:@"CouponId" ]];
    dashboard.campeignID = [NSString stringWithFormat:@"%@",[[_arrContent objectAtIndex:index]valueForKey:@"ScheduleId" ]];
    [self addChildViewController:dashboard];
    [dashboard didMoveToParentViewController:self];
    [self.view addSubview:dashboard.view];
}


#pragma mark -
#pragma mark Custom Methods

- (void)updateCustomPageViewController {
    
    if (_scrollView.subviews.count > 0) {
        for (UIView *vx in _scrollView.subviews) {
            [vx removeFromSuperview];
        }
    }

    [self createCustomPageViewController];
}


- (void)createCustomPageViewController
{
    [self.view layoutIfNeeded];
    [self.scrollSuperView layoutIfNeeded];
    
    [appDelegate stopIndicator];
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH - 16),self.scrollSuperView.frame.size.height - 20)];
    if (IS_IPHONE_X) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH ), SCREEN_HEIGHT - 138)];
    }
    _scrollView.delegate = self;
    _scrollView.scrollEnabled = YES;
    _scrollView.userInteractionEnabled = YES;
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    self.scrollSuperView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.scrollSuperView.layer.shadowOpacity = 1;
    self.scrollSuperView.layer.shadowOffset = CGSizeZero;
    self.scrollSuperView.layer.shadowRadius = 2;
    self.scrollSuperView.layer.masksToBounds = NO;
    [self.scrollSuperView addSubview:_scrollView];
    
    _scrollView.contentSize = CGSizeMake(self.arrContent.count * (SCREEN_WIDTH - 16), self.scrollSuperView.frame.size.height - 20);
    if (IS_IPHONE_X) {
        _scrollView.contentSize = CGSizeMake(self.arrContent.count * (SCREEN_WIDTH ), SCREEN_HEIGHT - 138);
    }

    int xAxis = 0;
    
    [self.scrollSuperView setClipsToBounds:YES];
    for (int i=0; i<_arrContent.count; i++) {
        
        @autoreleasepool {
            
            UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(xAxis, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
            [viewContent setTag:i+1];
            [_scrollView addSubview:viewContent];
            [viewContent layoutIfNeeded];
            
            UIImageViewAligned *imageView = [[UIImageViewAligned alloc] initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height - 130)];
            [imageView setTag:i+5000];
            [imageView setContentMode:UIViewContentModeScaleAspectFit];
            imageView.alignTop = YES;
            imageView.clipsToBounds = YES;
            [viewContent addSubview:imageView];
            

            if (([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Primary"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Reward"]) && [[[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponText"] rangeOfString:@"\n"].location != NSNotFound) {
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height - 140)];
            }
            
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Survey"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Html"] || ![[[self.arrContent objectAtIndex:i] allKeys] containsObject:@"BarcodeUrl"]) {
                
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height - 20)];
            }
            
            if (![[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] && [[[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponText"] isEqualToString:@""]) {
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  imageView.frame.size.height+17)];
            }
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"BarcodeUrl"]  isEqual: @""] || [[self.arrContent objectAtIndex:i] objectForKey:@"BarcodeUrl"] == nil) {
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height - 20)];
                
            }

            
            UILabel *lblExpireDate = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.frame.size.height+5, viewContent.frame.size.width, 17)];

            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponText"] isEqualToString:@""]) {
                lblExpireDate.frame = CGRectMake(lblExpireDate.frame.origin.x, lblExpireDate.frame.origin.y, lblExpireDate.frame.size.width, 0);
            }
            else if ([[[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponText"] rangeOfString:@"\n"].location != NSNotFound && [[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponText"] != nil) {
                lblExpireDate.frame = CGRectMake(lblExpireDate.frame.origin.x, lblExpireDate.frame.origin.y, lblExpireDate.frame.size.width, 38);
            }
            else if ([[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponText"] == nil){
                lblExpireDate.frame = CGRectMake(lblExpireDate.frame.origin.x, lblExpireDate.frame.origin.y, lblExpireDate.frame.size.width, 0);
            }
            
            [lblExpireDate setFont:[UIFont systemFontOfSize:15.0]];
            [lblExpireDate setTextAlignment:NSTextAlignmentCenter];
            [lblExpireDate setTag:i+6000];
            [lblExpireDate setNumberOfLines:0];
            [lblExpireDate setText:[[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponText"]];
            [viewContent addSubview:lblExpireDate];
            
            
            UIImageView *imageViewBarcode = [[UIImageView alloc] initWithFrame:CGRectMake((viewContent.frame.size.width - (viewContent.frame.size.width - 80))/2, lblExpireDate.frame.origin.y + lblExpireDate.frame.size.height+5, viewContent.frame.size.width - 80, 70)];
            [imageViewBarcode setTag:i+7000];
            [imageViewBarcode setContentMode:UIViewContentModeScaleAspectFit];
            [viewContent addSubview:imageViewBarcode];
            [imageViewBarcode sd_setImageWithURL:[NSURL URLWithString:[[self.arrContent objectAtIndex:i] objectForKey:@"BarcodeUrl"]]];

            
            UILabel *lblCouponText = [[UILabel alloc] initWithFrame:CGRectMake(8, imageViewBarcode.frame.origin.y + imageViewBarcode.frame.size.height+ 5, viewContent.frame.size.width-16, 17)];
            [lblCouponText setTag:i+8000];
            [lblCouponText setFont:[UIFont systemFontOfSize:15.0]];
            [lblCouponText setTextAlignment:NSTextAlignmentCenter];
            [lblCouponText setText:[[self.arrContent objectAtIndex:i] objectForKey:@"BottomCouponText"]];
            [viewContent addSubview:lblCouponText];
            
            
            UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] init];
            activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            activityView.color = [UIColor grayColor];
            activityView.tag = i+9000;
            activityView.center = CGPointMake(CGRectGetMidX(viewContent.bounds), CGRectGetMidY(viewContent.bounds));
            [viewContent addSubview:activityView];
            
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"]) {
                
                UILabel *lblAmounLeft = [[UILabel alloc] initWithFrame:CGRectMake(8, lblCouponText.frame.origin.y + lblCouponText.frame.size.height, viewContent.frame.size.width-16, 17)];
                [lblAmounLeft setTag:i+10000];
                [lblAmounLeft setFont:[UIFont systemFontOfSize:15.0]];
                lblAmounLeft.textAlignment = NSTextAlignmentCenter;
                lblAmounLeft.text = [NSString stringWithFormat:@"%d", [[[self.arrContent objectAtIndex:i] objectForKey:@"AmountLeft"] intValue]];
                [viewContent addSubview:lblAmounLeft];
            }
            
            [activityView startAnimating];
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Survey"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Html"]) {
                
                [imageView sd_setImageWithURL:[NSURL URLWithString:[[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponImageUrl"]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [activityView stopAnimating];
                    [activityView hidesWhenStopped];
                }];
            } else {
                [imageView sd_setImageWithURL:[NSURL URLWithString:[[self.arrContent objectAtIndex:i] objectForKey:@"TopCouponImageUrl"]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [activityView stopAnimating];
                    [activityView hidesWhenStopped];
                }];
            }
            
            UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewContentTapGuesture:)];
            tapGuesture.numberOfTapsRequired = 1;
            [viewContent addGestureRecognizer:tapGuesture];
            
            xAxis += _scrollView.frame.size.width;
        }
    }
    
    if (_arrContent.count == 0) {
        
        UILabel *noLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, (self.scrollSuperView.frame.size.height - 50)/2, self.scrollSuperView.frame.size.width - 20, 50)];
        noLabel.text = @"Coming soon";
        noLabel.textColor = [UIColor grayColor];
        noLabel.font = [UIFont fontWithName:GeorgeRegular size:16.0];
        noLabel.textAlignment = NSTextAlignmentCenter;
        [self.scrollSuperView addSubview:noLabel];
    }
    else{
        
    }
    
    
    //changes done by jitender
    BOOL isShare = false;
    if (self.arrContent.count>0) {
        isShare = [[[self.arrContent objectAtIndex:self.initialIndex] objectForKey:@"Shareable"] boolValue];
    }
    
    if (isShare) { self.btnShareView.hidden = NO; }
    else {
        self.btnShareView.hidden = YES;
    }
    
    _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width * self.initialIndex, 0);
    self.pageControl.numberOfPages = self.arrContent.count;

    self.pageControl.currentPage = self.initialIndex;
    [self.scrollSuperView bringSubviewToFront:self.pageControl];
}

- (void)viewContentTapGuesture:(UITapGestureRecognizer *)guesture
{
    UIView *viewContent = (UIView *)guesture.view;
    
    if (viewContent.tag >= 100) {
        
        UIImageView *imageCoupon = (UIImageView *)[self.view viewWithTag:((viewContent.tag/100) + 5000 -1)];
        
        if (![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] allKeys] containsObject:@"BarcodeUrl"]) {
            [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  viewContent.frame.size.height-20)];
        }
        else if (![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopCouponText"] isEqualToString:@""] && ![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Primary"]) {
            [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  imageCoupon.frame.size.height+17)];
        }
        else if (([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Primary"] || [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Reward"]) && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopCouponText"] rangeOfString:@"\n"].location != NSNotFound) {
            [imageCoupon setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height-140)];
        }
        else if ([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Primary"] && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopCouponText"] isEqualToString:@""]) {
            [imageCoupon setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height-100)];
        }
        else {
            [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  viewContent.frame.size.height-130)];
        }
        
        [self flipAnimationOnView:viewContent withImageView:imageCoupon withFGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopCouponImageUrl"] andBGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"BackUrl"]];
    }
    else {
        
        UIImageView *imageCoupon = (UIImageView *)[self.view viewWithTag:(viewContent.tag+5000)-1];
        
        if ([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] allKeys] containsObject:@"BackUrl"] && ![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"BackUrl"] isEqualToString:@""]) {
            
            [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  viewContent.frame.size.height-20)];
            
            [self flipAnimationOnView:viewContent withImageView:imageCoupon withFGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopCouponImageUrl"] andBGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"BackUrl"]];
        }
        else if ([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Html"] || [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Survey"]) {
            
//            // Show webview.
            KioskWebViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webController"];
            viewController.urlString = [[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"OverlayUrl"];
            [self presentViewController:viewController animated:YES completion:nil];
            
        }
    }
}

- (void)flipAnimationOnView:(UIView *)viewContent withImageView:(UIImageView *)imgView withFGImg:(NSString *)forgroundImageURL andBGImg:(NSString *)backgroundImageURL
{
    [UIView transitionWithView:viewContent duration:.3 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        
        if (viewContent.tag < 100) {
            
            viewContent.tag = viewContent.tag * 100;
            [imgView sd_setImageWithURL:[NSURL URLWithString:backgroundImageURL]];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            
            [self hideFiveElements:YES andTag:imgView.tag];
        }
        else {
            
            viewContent.tag = viewContent.tag / 100;
            [imgView sd_setImageWithURL:[NSURL URLWithString:forgroundImageURL]];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            
            [self hideFiveElements:NO andTag:imgView.tag];
        }
    } completion:nil];
}

- (void)hideFiveElements:(BOOL)status andTag:(NSInteger)tagValue {
    
    UILabel *lblExpireDate = (UILabel *)[self.view viewWithTag:tagValue+1000];
    [lblExpireDate setHidden:status];
    
    UIImageView *imageViewBarcode = (UIImageView *)[self.view viewWithTag:tagValue+2000];
    [imageViewBarcode setHidden:status];
    
    UILabel *lblCouponText = (UILabel *)[self.view viewWithTag:tagValue+3000];
    [lblCouponText setHidden:status];
    
    UILabel *lblAmounLeft = (UILabel *)[self.view viewWithTag:tagValue+5000];
    [lblAmounLeft setHidden:status];
}

- (void)showLodingIndicator:(BOOL)show {
    
    if (show) { // SHOW
        
        UIActivityIndicatorView * refreshIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshIndicator.frame = CGRectMake(self.btnRefresh.frame.origin.x, self.btnRefresh.frame.origin.y, self.btnRefresh.frame.size.width, self.btnRefresh.frame.size.height);
        refreshIndicator.hidesWhenStopped = YES;
        refreshIndicator.tag = 1234;
    
        [self.view addSubview:refreshIndicator];
        
        [refreshIndicator startAnimating];
        self.btnRefresh.hidden = YES;
        
    } else { // HIDE
        
        UIActivityIndicatorView * refreshIndicator = (UIActivityIndicatorView *)[self.view viewWithTag:1234];
        if (refreshIndicator.isAnimating) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [refreshIndicator stopAnimating];
                self.btnRefresh.hidden = NO;
            });
        }
        [refreshIndicator removeFromSuperview];
        refreshIndicator = nil;
    }
}

- (IBAction)SideButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController toggleSideMenuView];
}

- (IBAction)refreshButtonPressed:(id)sender {
    
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    
    _btnRefresh.hidden = YES;
    [self.imgHeader addSubview:refreshActivityView];
    
    
    int index = _scrollView.contentOffset.x / _scrollView.frame.size.width;
    
    UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)[self.view viewWithTag:index+9000];
    [activityView startAnimating];
    
    
    [self hideFiveElements:YES andTag:index+5000];
    
    
    if (_arrContent.count > 0) {
        [[SDImageCache sharedImageCache] removeImageForKey:[[self.arrContent objectAtIndex:index] valueForKey:@"BarcodeUrl"] fromDisk:YES];
        [[SDImageCache sharedImageCache] removeImageForKey:[[self.arrContent objectAtIndex:index] valueForKey:@"TopCouponImageUrl"] fromDisk:YES];
    }
    
    [appDelegate startIndicator];

    [self updateOffers];
}

- (IBAction)changePage:(id)sender {
    
    _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width*self.pageControl.currentPage+1, 0);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int currentIndex = _scrollView.contentOffset.x / _scrollView.frame.size.width;
    self.pageControl.currentPage = currentIndex;
    self.initialIndex = currentIndex;
    
    // Hide or Show share button
    
    BOOL isShare = [[[self.arrContent objectAtIndex:currentIndex] objectForKey:@"Shareable"] boolValue];
    if (isShare) { self.btnShareView.hidden = NO; }
    else { self.btnShareView.hidden = YES; }
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}


- (CGRect)getFrameOfImage:(UIImageView *) imgView
{
    if(!imgView)
        return CGRectZero;
    
    CGSize imgSize      = imgView.image.size;
    CGSize frameSize    = imgView.frame.size;
    
    CGRect resultFrame;
    
    if(imgSize.width < frameSize.width && imgSize.height < frameSize.height)
    {
        resultFrame.size    = imgSize;
    }
    else 
    {
        float widthRatio  = imgSize.width  / frameSize.width;
        float heightRatio = imgSize.height / frameSize.height;
        
        float maxRatio = MAX (widthRatio , heightRatio);
        
        resultFrame.size = CGSizeMake(imgSize.width / maxRatio, imgSize.height / maxRatio);
    }
    
    resultFrame.origin  = CGPointMake(imgView.center.x - resultFrame.size.width/2 , imgView.center.y - resultFrame.size.height/2);
    
    return resultFrame;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
        CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
        if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//            MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//            [mo showHome];
            [self GoHome:nil];
        }
}


-(void)GoHome:(NSNotification *)noti
{
//    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//    [mo showHome];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ScreenShot
//
//-(void)DidTakeScreen:(NSNotification *)noti{
//    //
//    //    PHPhotoLibrary *assetLibrary = [[PHPhotoLibrary alloc] init];
//    //    [assetLibrary performChanges:^{
//    //        <#code#>
//    //    } completionHandler:^(BOOL success, NSError * _Nullable error) {
//    //        <#code#>
//    //    }]
//    
//    
//    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
//    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
//    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
//    PHAsset *lastAsset = [fetchResult lastObject];
//    [[PHImageManager defaultManager] requestImageForAsset:lastAsset
//                                               targetSize:[[UIScreen mainScreen] bounds].size
//                                              contentMode:PHImageContentModeAspectFill
//                                                  options:PHImageRequestOptionsVersionCurrent
//                                            resultHandler:^(UIImage *result, NSDictionary *info) {
//                                                
//                                                NSLog(@"%@",result);
//                                                
//                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                    
////                                                    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
////                                                        [PHAssetChangeRequest creationRequestForAssetFromImage:[UIImage imageNamed:@"SplashBg"]];
////                                                    } completionHandler:^(BOOL success, NSError *error) {
////                                                        if (success) {
////                                                            NSLog(@"Success");
////                                                        }
////                                                        else {
////                                                            NSLog(@"write error : %@",error);
////                                                        }
////                                                    }];
//                                                    
//                                                    [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
//                                                        [PHAssetChangeRequest deleteAssets:[NSArray arrayWithObjects:lastAsset, nil]];
//                                                        
//                                                    } completionHandler:^(BOOL success, NSError * _Nullable error) {
//                                                        if (success) {
//                                                            NSLog(@"Success");
//                                                        }
//                                                        else {
//                                                            NSLog(@"write error : %@",error);
//                                                        }
//                                                    }];
//                                                });
//                                            }];
//}

@end
