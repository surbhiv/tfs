//
//  FeedbackViewController.h
//  RConnect
//
//  Created by HiteshDhawan on 27/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  FeedbackViewControllerDelegate<NSObject>

- (void)setBadgeValueForButtonThree:(int)badgeValue;

@end

@interface FeedbackViewController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) UILabel *screenNumber;
@property (strong, nonatomic) NSDictionary *dicFeedback;
@property (assign, nonatomic) id<FeedbackViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *lblShoppingExp;
@property (weak, nonatomic) IBOutlet UILabel *lblShoppingTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblRateEnds;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblRateMore;
@property (weak, nonatomic) IBOutlet UIButton *btnRate;
@property (weak, nonatomic) IBOutlet UILabel *lblStoreName;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRate;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIView *headerView;



- (IBAction)starRatingButtonPressed:(id)sender;
- (IBAction)submitRateButtonPressed:(id)sender;
- (IBAction)refreshButtonPressed:(id)sender;
@end
