//
//  ContactUsVC.h
//  TFS
//
//  Created by Surbhi on 26/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UIView *viewContact;

@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *contactButton;

- (IBAction)NUMBERPressed:(id)sender;
- (IBAction)EMAILPressed:(id)sender;

- (IBAction)FBPressed:(id)sender;
- (IBAction)TwitterPressed:(id)sender;
- (IBAction)InstaGramPressed:(id)sender;



@end
