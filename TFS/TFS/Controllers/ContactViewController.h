//
//  ContactViewController.h
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFS.pch"

@protocol ContactViewControllerDelegate <NSObject>

- (void)selectedPersonContact:(NSString *)phoneNumber;

@end

@interface ContactViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate>
@property (nonatomic, strong) NSArray *arrSearchContacts;
@property (nonatomic, strong) NSMutableArray *arrContacts;
@property (nonatomic,strong) IBOutlet UITableView *tblContacts;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;


@property (assign, nonatomic) id<ContactViewControllerDelegate>delegate;

- (IBAction)backButtonPressed:(id)sender;
@end
