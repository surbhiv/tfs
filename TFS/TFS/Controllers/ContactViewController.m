//
//  ContactViewController.m
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "ContactViewController.h"
#import <AddressBook/AddressBook.h>
#import "TFS.pch"

@interface ContactViewController ()

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.arrContacts = [NSMutableArray array];
    
    [appDelegate startIndicator];
    [NSThread detachNewThreadSelector:@selector(getPersonOutOfAddressBook) toTarget:self withObject:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}
- (void)getPersonOutOfAddressBook
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
//    CNEntityType entityType = CNEntityTypeContacts;
//    if ([CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined) {
//        CNContactStore *addbookref = [[CNContactStore alloc]init];
//        [addbookref requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
//            if(granted){
//                [self getAllContact];
//            }
//        }];
//    }
//    else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized)
//    {
//        [self getAllContact];
//    }
//    else{
//        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Contacts Alert!" andMessage:@"Change privacy setting to get the list of contacts in settings" andButtonTitle:@"Ok"];
//
//    }
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            ABAddressBookRef addressBook = ABAddressBookCreate();
//            NSLog(@"Created address book ref: %@", addressBook);
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
//
        CFErrorRef *error = NULL;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
        CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
        
        NSUInteger i = 0;
        for (i = 0; i < numberOfPeople; i++) {
            @autoreleasepool {
                NSMutableDictionary *person = [NSMutableDictionary dictionary];
                
                ABRecordRef contactPerson = CFArrayGetValueAtIndex(allPeople, i);
                
                NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty));
                NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(contactPerson, kABPersonLastNameProperty));
                
                NSString *fullName = firstName;
                if (lastName) {
                    fullName = [NSString stringWithFormat:@"%@ %@", fullName, lastName];
                }
                
                if (firstName) {
                    [person setObject:firstName forKey:@"FistName"];
                }
                if (lastName) {
                    [person setObject:lastName forKey:@"LastName"];
                }
                if (fullName) {
                    [person setObject:fullName forKey:@"FullName"];
                }
                
                // email
                ABMultiValueRef emails = ABRecordCopyValue(contactPerson,
                                                           kABPersonEmailProperty);
                NSUInteger j = 0;
                for (j = 0; j < ABMultiValueGetCount(emails); j++) {
                    @autoreleasepool {
                        NSString *email = (__bridge_transfer NSString
                                           *)ABMultiValueCopyValueAtIndex(emails, j);
                        if (j == 0) {
                            
                            if (email) {
                                [person setObject:email forKey:@"HomeEmail"];
                            }
                        }
                        else if (j==1) {
                            if (email) {
                                [person setObject:email forKey:@"WorkEmail"];
                            }
                        }
                    }
                }
                
                ABMultiValueRef phoneNumbers = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
                
                for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
                    @autoreleasepool {
                        NSString *phoneNumber = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                        if (phoneNumber) {
                            [person setObject:phoneNumber forKey:@"PhoneNumber"];
                        }
                    }
                }
                
                [self.arrContacts addObject:person];
                [self.tblContacts reloadData];
//                [SVProgressHUD dismiss];
                [appDelegate stopIndicator];
                
            }
        }
        
        if (!(numberOfPeople > 0)) {
//            [SVProgressHUD dismiss];
            [appDelegate stopIndicator];
        }
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Contacts Alert!" andMessage:@"Change privacy setting to get the list of contacts in settings" andButtonTitle:@"Ok"];
    }
    
    CFRelease(addressBookRef);
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"FullName contains[c] %@", searchText];
    self.arrSearchContacts = [self.arrContacts filteredArrayUsingPredicate:resultPredicate];
}


#pragma mark -
#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.arrSearchContacts count];
    }
    return [self.arrContacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ContactCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        cell.textLabel.text = [[self.arrSearchContacts objectAtIndex:indexPath.row] objectForKey:@"FullName"];
        cell.detailTextLabel.text = [[self.arrSearchContacts objectAtIndex:indexPath.row] objectForKey:@"PhoneNumber"];
    }
    else {
        
        cell.textLabel.text = [[self.arrContacts objectAtIndex:indexPath.row] objectForKey:@"FullName"];
        cell.detailTextLabel.text = [[self.arrContacts objectAtIndex:indexPath.row] objectForKey:@"PhoneNumber"];
    }
    
    return cell;
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedPersonContact:)]) {
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            
            if (![[[self.arrSearchContacts objectAtIndex:indexPath.row] objectForKey:@"PhoneNumber"] isEqualToString:@""]) {
                [self.delegate selectedPersonContact:[[self.arrSearchContacts objectAtIndex:indexPath.row] objectForKey:@"PhoneNumber"]];
            }
        }
        else {
            
            if (![[[self.arrContacts objectAtIndex:indexPath.row] objectForKey:@"PhoneNumber"] isEqualToString:@""]) {
                [self.delegate selectedPersonContact:[[self.arrContacts objectAtIndex:indexPath.row] objectForKey:@"PhoneNumber"]];
            }
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark UISearchDisplayDelegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


- (IBAction)backButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getAllContact
{
    if([CNContactStore class])
    {
        //iOS 9 or later
        NSError* contactError;
        CNContactStore* addressBook = [[CNContactStore alloc]init];
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey];
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        BOOL success = [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            [self parseContactWithContact:contact];
        }];
        
        if (success) {
            
            
        }
    }
}

-(void)getContacts
{
    if([CNContactStore class])
    {
        //iOS 9 or later
        NSError* contactError;
        CNContactStore* addressBook = [[CNContactStore alloc]init];
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey];
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        BOOL success = [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            [self parseContactWithContact:contact];
        }];
        
        if (success) {
            
            
        }
    }
}

- (void)parseContactWithContact :(CNContact* )contact
{
//    NSString * firstName =  contact.givenName;
//    NSString * lastName =  contact.familyName;
//    NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
//    NSString * email = [contact.emailAddresses valueForKey:@"value"];
//    NSArray * addrArr = [self parseAddressWithContac:contact];
}

//- (NSMutableArray *)parseAddressWithContac: (CNContact *)contact
//{
//    NSMutableArray * addrArr = [[NSMutableArray alloc]init];
//    CNPostalAddressFormatter * formatter = [[CNPostalAddressFormatter alloc]init];
//    NSArray * addresses = (NSArray*)[contact.postalAddresses valueForKey:@"value"];
//    if (addresses.count > 0) {
//        for (CNPostalAddress* address in addresses) {
//            [addrArr addObject:[formatter stringFromPostalAddress:address]];
//        }
//    }
//    return addrArr;
//}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        [mo showHome];
    }
}
@end
