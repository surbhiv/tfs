//
//  MoreViewController.m
//  k kiosk
//
//  Created by HiteshDhawan on 23/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "MoreViewController.h"
#import "TFS.pch"

#define MyDateTimePickerHeight 260
#define SIZE [[UIScreen mainScreen] bounds].size


@interface MoreViewController ()<UIWebViewDelegate, MFMailComposeViewControllerDelegate, UITextFieldDelegate>

@end


@implementation MoreViewController

#pragma mark -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [self.txtName setRightPadding:30  withImageName:@"edit_ic"];
    
    
    self.viewProfile.layer.cornerRadius = 3.0;
    self.viewProfile.clipsToBounds = YES;
    self.viewProfile.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.viewProfile.layer.shadowOpacity = 1;
    self.viewProfile.layer.shadowOffset = CGSizeZero;
    self.viewProfile.layer.shadowRadius = 1.0;
    self.viewProfile.layer.masksToBounds = NO;
    
    [self disableTextfields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.btnSaveChanges setTitle:@"EDIT PROFILE" forState:UIControlStateNormal];
    
    
    // Load user profile
    NSString *strURL = [NSString stringWithFormat:@"%@user/%@", BASEURL, appDelegate.strUserId];
    [self loadUserProfile:strURL];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"PROFILE"],
                                     kFIRParameterItemName:@"PROFILE",
                                     kFIRParameterContentType:@""
                                     }];

}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    
    for (ASIHTTPRequest *request in ASIHTTPRequest.sharedQueue.operations) {
        if (![request isCancelled]) {
            [request cancel];
            [request setDelegate:nil];
        }
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)toggleSideMenuView:(id)sender
{
//    [self.navigationController toggleSideMenuView];
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.btnSaveChanges setTitle:@"UPDATE" forState:UIControlStateNormal];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual: self.txtPin]) {
        if (range.location > 5) {
            return NO;
        }
    }
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark Custom Methods

- (void)dismissKeyboard
{
//    [self.txtDateOfBirth resignFirstResponder];
    [self.txtMobileNo resignFirstResponder];
    [self.txtName resignFirstResponder];
}

#pragma mark -
#pragma mark IBAction Methods

- (IBAction)saveChangesButtonPressed:(id)sender {

    if ([self.btnSaveChanges.titleLabel.text isEqualToString:@"EDIT PROFILE"]) {
        [self EnableTextfields];
        [self.btnSaveChanges setTitle:@"UPDATE" forState:UIControlStateNormal];
    }
    else{
        // Update user profile
        
        if (self.txtName.text.length == 0 || [self.txtName.text isEqualToString:@""]) {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please Enter Name."];
        }
        else if (![HelperClass isValidEmail:self.txtEmail.text] || self.txtEmail.text.length == 0 || [self.txtEmail.text isEqualToString:@""]) {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please Enter a valid Email Address."];
        }
        else if (self.txtCity.text.length == 0 || [self.txtCity.text isEqualToString:@""]) {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please Enter City."];
        }
        else if (self.txtAddress.text.length == 0 || [self.txtCity.text isEqualToString:@""]) {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please Enter Address."];
        }
        else if (self.txtPin.text.length == 0 || [self.txtPin.text isEqualToString:@""] || self.txtPin.text.length < 6) {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please Enter Valid PostCode."];
        }
        else{
            [self SaveChangesMethod];
        }
    }
}


-(void)SaveChangesMethod{
    [self dismissKeyboard];
    [appDelegate startIndicator];
    
    NSString *strURL = [NSString stringWithFormat:@"%@user", BASEURL];
    
    NSString *_dateString = [HelperClass getDateString];
    
    // Generate signature
    NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
    NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
    
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:strURL]];
    [request setRequestMethod:@"PUT"];
    request.name = @"SaveChanges";
    [request setTimeOutSeconds:30.0];
    [request setDelegate:self];
    
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
    [request addRequestHeader:@"X-Liquid-Signature" value:signature];
    
    NSString *pin = [NSString stringWithFormat:@"%@",_txtPin.text];
    NSString *add = (_txtAddress.text) ? [NSString stringWithFormat:@"%@",_txtAddress.text] : @" ";
    NSString *cit = (_txtCity.text) ? [NSString stringWithFormat:@"%@",_txtCity.text] : @" ";
    NSString *mail = (_txtEmail.text) ? [NSString stringWithFormat:@"%@",_txtEmail.text] : @"";
    
    NSMutableDictionary *paramDic;
    if ([pin isEqualToString:@""]) {
        
        paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                     @"Name":_txtName.text,
                                                                     @"DateOfBirth":_txtDateOfBirth.text,
                                                                     @"Email":mail,
                                                                     @"City":cit,
                                                                     @"Address":add}];
    }
    else {
        
        paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                     @"Name":_txtName.text,
                                                                     @"DateOfBirth":_txtDateOfBirth.text,
                                                                     @"Email":mail,
                                                                     @"City":cit,
                                                                     @"Address":add,
                                                                     @"PostCode":pin}];
    }
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
    [request setPostBody:[NSMutableData dataWithData:data]];
    
    [request startAsynchronous];

}

#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadUserProfile:(NSString *)urlString {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [appDelegate stopIndicator];
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    
    if ([request.name isEqualToString:@"SaveChanges"]) {
        NSString *strURL = [NSString stringWithFormat:@"%@user/%@", BASEURL, appDelegate.strUserId];
        [self loadUserProfile:strURL];
    }
    else{
        NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
        
        [self disableTextfields];
        NSLog(@"%@",dicUser);
        [self.btnSaveChanges setTitle:@"EDIT PROFILE" forState:UIControlStateNormal];
        
        // Assign values to textfields with validation
        if ([[dicUser allKeys] containsObject:@"Msn"]) {
            self.txtMobileNo.text = [dicUser objectForKey:@"Msn"];
            [[NSUserDefaults standardUserDefaults] setObject:[dicUser objectForKey:@"Msn"] forKey:@"MSN"];
        }
        if ([[dicUser allKeys] containsObject:@"DateOfBirth"]) {
            NSString *date = [dicUser objectForKey:@"DateOfBirth"];
            self.txtDateOfBirth.text = date;
            self.txtDateOfBirth.hidden = NO;
            [[NSUserDefaults standardUserDefaults] setObject:[dicUser objectForKey:@"DateOfBirth"] forKey:@"DateOfBirth"];
        }
        
        if ([[dicUser allKeys] containsObject:@"Name"]) {
            self.txtName.text = [dicUser objectForKey:@"Name"];
        }
        
        if ([[dicUser allKeys] containsObject:@"Address"]) {
            self.txtAddress.text = [dicUser objectForKey:@"Address"];
        }
        
        if ([[dicUser allKeys] containsObject:@"City"]) {
            self.txtCity.text = [dicUser objectForKey:@"City"];
        }
        
        if ([[dicUser allKeys] containsObject:@"PostCode"]) {
            self.txtPin.text = [dicUser objectForKey:@"PostCode"];
        }
        
        if ([[dicUser allKeys] containsObject:@"Email"]) {
            self.txtEmail.text = [dicUser objectForKey:@"Email"];
        }

    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
//    [SVProgressHUD dismiss];
    [appDelegate stopIndicator];
    
    [self disableTextfields];
    [self.btnSaveChanges setTitle:@"EDIT PROFILE" forState:UIControlStateNormal];

    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];

}



#pragma mark - Miscelleneous

- (NSString *) dateStringFromString :(NSString *) dateString {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ss aa"];
    NSDate *date = [dateFormat dateFromString:dateString];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd/MMMM/yyyy"];
    return [dateFormat stringFromDate:date];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//        [mo showHome];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    [self.view endEditing:YES];
}


#pragma mark - TEXTFIELD DISABLE AND ENABLE

-(void)disableTextfields
{
    self.txtName.userInteractionEnabled = NO;
    self.txtEmail.userInteractionEnabled = NO;
    self.txtCity.userInteractionEnabled = NO;
    self.txtAddress.userInteractionEnabled = NO;
    self.txtPin.userInteractionEnabled = NO;
    
    [self setImageOntextField:self.txtName andImage:@"" withSize:CGSizeMake(0, 0) ];
    [self setImageOntextField:self.txtEmail andImage:@"" withSize:CGSizeMake(0, 0) ];
    [self setImageOntextField:self.txtCity andImage:@"" withSize:CGSizeMake(0, 0) ];
    [self setImageOntextField:self.txtAddress andImage:@"" withSize:CGSizeMake(0, 0) ];
    [self setImageOntextField:self.txtPin andImage:@"" withSize:CGSizeMake(0, 0) ];

}

-(void)EnableTextfields
{
    self.txtName.userInteractionEnabled = YES;
    self.txtEmail.userInteractionEnabled = YES;
    self.txtCity.userInteractionEnabled = YES;
    self.txtAddress.userInteractionEnabled = YES;
    self.txtPin.userInteractionEnabled = YES;
    
    
    [self setImageOntextField:self.txtName andImage:@"editPen" withSize:CGSizeMake(15, 15) ];
    [self setImageOntextField:self.txtEmail andImage:@"editPen" withSize:CGSizeMake(15, 15) ];
    [self setImageOntextField:self.txtCity andImage:@"editPen" withSize:CGSizeMake(15, 15) ];
    [self setImageOntextField:self.txtAddress andImage:@"editPen" withSize:CGSizeMake(15, 15) ];
    [self setImageOntextField:self.txtPin andImage:@"editPen" withSize:CGSizeMake(15, 15) ];

}


-(void)setImageOntextField:(UITextField *)textfield andImage:(NSString *)image withSize:(CGSize)size{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, size.width, size.height)];
    imv.image = [UIImage imageNamed:image];
    textfield.rightViewMode = UITextFieldViewModeAlways;
    textfield.rightView = imv;
}

@end
