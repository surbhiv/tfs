//
//  DetailsViewController.h
//  TwentyFourSeven
//
//  Created by Hitesh Dhawan on 06/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CouponDetailsViewControllerDelegate <NSObject>

@optional
- (void)couponDetailDidBackFromCouponId:(NSInteger)couponID;

@end


@interface CouponDetailsViewController : UIViewController <UIScrollViewDelegate, UITabBarControllerDelegate, CouponDetailsViewControllerDelegate>

@property (nonatomic, strong) NSArray *arrContent;
@property (nonatomic, assign) NSInteger couponId;
@property (nonatomic, assign) NSInteger initialIndex;

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UIView *imgHeader;
//@property (weak, nonatomic) IBOutlet UILabel *noContentLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIView *btnShareView;

@property (weak, nonatomic) IBOutlet UIView *scrollSuperView;


@property (nonatomic, assign) id<CouponDetailsViewControllerDelegate> delegate;

- (IBAction)changePage:(id)sender;
- (IBAction)SideButtonPressed:(id)sender;
- (IBAction)shareButtonPressed:(id)sender;
- (IBAction)refreshButtonPressed:(id)sender;

@property int selectedPageOnDetail;
@property BOOL isComingFromShare;

@end
