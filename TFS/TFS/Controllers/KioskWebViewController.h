//
//  KioskWebViewController.h
//  k kiosk
//
//  Created by HiteshDhawan on 03/12/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KioskWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSString *urlString;

@property (weak, nonatomic) IBOutlet UIWebView *kwebView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbitemBack;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbitemForward;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbitemRefresh;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;


- (IBAction)backButtonPressred:(id)sender;
- (IBAction)CancelButtonPressred:(id)sender;
@end
