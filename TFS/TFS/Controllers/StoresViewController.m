//
//  StoresViewController.m
//  RConnect
//
//  Created by HiteshDhawan on 27/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "StoresViewController.h"
#import "StoreCell.h"
#import "StoreDetailsViewController.h"
#import "TFS.pch"

@interface StoresViewController () < CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    UIRefreshControl *refreshControl;
    UIBarButtonItem *rightButton;
    UIActivityIndicatorView *refreshActivityView;
    
    CLLocation *currentLocation;
    CLLocationManager *locationManager;
}

@end

@implementation StoresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Get user current location.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];

    self.searchView.layer.cornerRadius = 8.0;
    self.searchView.clipsToBounds = YES;
    
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};

    self.tblStores.estimatedRowHeight = 215;
    self.tblStores.rowHeight = UITableViewAutomaticDimension;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [self.tblStores addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    
    self.arrStores = [[NSUserDefaults standardUserDefaults] objectForKey:@"Stores"];

    [appDelegate startIndicator];
    [self performSelector:@selector(LoadTableMethod) withObject:nil afterDelay:3.0];

}

- (void)dismissKeyboard
{
    [self.searchTxtField resignFirstResponder];
}


- (IBAction)toggleSideMenuView:(id)sender
{
//    [self.navigationController toggleSideMenuView];
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)LoadTableMethod{
    
    if (self.arrStores.count > 0) {
        
        [appDelegate stopIndicator];
        [self SortStoresBasisOnDistance:self.arrStores];
    }
    else if (appDelegate.arrStores.count > 0) {
        
        [appDelegate stopIndicator];
        self.arrStores = appDelegate.arrStores;
        [self SortStoresBasisOnDistance:self.arrStores];
    }
    else {
        [appDelegate startIndicator];
        [self loadStores];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (!([self.arrStores count] > 0)) {
        
        [appDelegate startIndicator];
        [self loadStores];
    }
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"STORE FINDER"],
                                     kFIRParameterItemName:@"STORE FINDER",
                                     kFIRParameterContentType:@""
                                     }];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadStores {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        //disabling back button till request not finished
        
        NSString *urlString = [NSString stringWithFormat:@"%@Stores/%@", BASEURL, appDelegate.strUserId];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinished:)];
        [request setDidFailSelector:@selector(requestFailed:)];
        [request startAsynchronous];

    }
    else {
        
        [_btnRefresh setHidden:NO];
        [refreshControl endRefreshing];
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        
        [appDelegate stopIndicator];

        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate.locationManager startUpdatingLocation];
    NSDictionary *dicStores = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicStores allKeys] containsObject:@"Stores"]) {

        NSMutableArray *storeArr = [[NSMutableArray alloc] initWithArray:[dicStores objectForKey:@"Stores"]];
        [self SortStoresBasisOnDistance:storeArr];
        
        // Put code here thats at bottom page
    }
    
    [appDelegate stopIndicator];
    [_btnRefresh setHidden:NO];
    [refreshControl endRefreshing];
    [refreshActivityView removeFromSuperview];
}

-(void)SortStoresBasisOnDistance:(NSArray *)dicStores{
    
    NSMutableArray *storeArray = [[NSMutableArray alloc] init];
    if (currentLocation != nil) {
        
        for (NSDictionary *dic in dicStores) {
            
            @autoreleasepool {
                NSMutableDictionary *dicObj = [[NSMutableDictionary alloc] initWithDictionary:dic];
                
                CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:[[dicObj objectForKey:@"Latitude"] floatValue] longitude:[[dicObj objectForKey:@"Longitude"] floatValue]];
                [dicObj setObject:[self kilometersFromPlace:currentLocation.coordinate andToPlace:storeLocation.coordinate] forKey:@"Distance"];
                
                // geofencing
                CLCircularRegion *circle = [[CLCircularRegion alloc] initWithCenter:[storeLocation coordinate] radius:100.0 identifier:dicObj[@"Name"]];
                
                circle.notifyOnEntry = YES;
                circle.notifyOnExit = YES;
                
                if (appDelegate.latestGeofences == nil) {
                    appDelegate.latestGeofences = [NSMutableArray array];
                }
                [appDelegate.latestGeofences addObject:circle];
                
                
                NSMutableArray *openArr = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"OpeningHours"]];;
                for (int i = 0; i < openArr.count; i++) {
                    
                    if (openArr[i]  == [NSNull null]) {
                        [openArr replaceObjectAtIndex:i withObject:@""];
                    }
                }
                
                [dicObj setObject:openArr forKey:@"OpeningHours"];
                [storeArray addObject:dicObj];
            }
        }
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Distance" ascending:YES];
        self.arrStores = [[NSArray alloc] initWithArray:[storeArray mutableCopy]];
        self.arrStores = [self.arrStores sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        // Save data to prefrences
        [[NSUserDefaults standardUserDefaults] setObject:self.arrStores forKey:@"Stores"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
    }
    else{
        
        self.arrStores = [[NSArray alloc] initWithArray:dicStores];
    }
    self.arr_SearchStores = [[NSMutableArray alloc] initWithArray:self.arrStores];
    [self.tblStores reloadData];

}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [_btnRefresh setHidden:NO];
    [refreshControl endRefreshing];
    [refreshActivityView removeFromSuperview];
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];

}


#pragma mark -
#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.arr_SearchStores count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"StoreCell";
    
    StoreCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.viewContent.layer.cornerRadius = 8.0;
    cell.viewContent.clipsToBounds = YES;
    
    cell.viewContent.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    cell.viewContent.layer.shadowOpacity = 1;
    cell.viewContent.layer.shadowOffset = CGSizeZero;
    cell.viewContent.layer.shadowRadius = 2.0;
    cell.viewContent.layer.masksToBounds = NO;
    
    
    NSString *storeNameSTR =  [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Name"];
    if (![storeNameSTR isEqual:[NSNull null]]) {
        cell.lblName.text = storeNameSTR;
        cell.storeName.text = storeNameSTR;
    }
    
//    NSString *storeImg = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Logo"];
//    if (![storeImg isEqual:[NSNull null]]  && storeImg != nil) {
//
//        cell.storeImage.contentMode = UIViewContentModeScaleAspectFit;
//        [cell.storeImage sd_setImageWithURL:[NSURL URLWithString:storeImg]
//                         placeholderImage:nil];
//    }
//    else{
        cell.storeImage.image = [UIImage imageNamed:@"locate.png"];

//    }
    
    
    NSString *address = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Address"];
    if (![address isEqual:[NSNull null]]) {
        cell.lblAddress.text = address;
    }
    
    NSString *distance = [NSString stringWithFormat:@"%.2f km", [[[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Distance"] floatValue]];
    if (![distance isEqual:[NSNull null]]) {
            cell.storeDistance.text = distance;
    }
    
    NSString *state = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"CurrentState"];
    if (![state isEqual:[NSNull null]]) {
        cell.storeState.text = state;
    }
    
    
    NSString *phone = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Telephone"];
    if (![state isEqual:[NSNull null]]) {
        cell.storePhone.text = phone;
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    StoreDetailsViewController *detailsController = [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreDetailsViewController"];
    detailsController.receivedDictionary = [self.arr_SearchStores objectAtIndex:indexPath.row];
    
    [self presentViewController:detailsController animated:NO completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 215.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


#pragma mark -
#pragma mark CLLocationManagerDelegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    currentLocation = [locations lastObject];
    if (currentLocation != nil) {
        [locationManager stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
//    NSLog(@"Failed to get user current location.");
}

-(void)get_SortedSearchArrayForText:(NSString *)str{
    
    if ([str isEqualToString:@""]) {
        self.arr_SearchStores = [[NSMutableArray alloc] initWithArray:self.arrStores];
        [self.tblStores reloadData];
    }
    else{
        
        
        self.arr_SearchStores = [NSMutableArray array];
        
        for (int i = 0; i < self.arrStores.count; i++) {
            NSDictionary *dc = [self.arrStores objectAtIndex:i];
            NSString *addStore = dc[@"Address"];
            NSString *nameStore = dc[@"Name"];

            NSRange range = [addStore  rangeOfString: str options: NSCaseInsensitiveSearch];
            NSRange range2 = [nameStore  rangeOfString: str options: NSCaseInsensitiveSearch];

            if ((range.location != NSNotFound) || (range2.location != NSNotFound)) {
                [self.arr_SearchStores addObject:dc];
            }
        }
        [self.tblStores reloadData];
    }
    
    
}


#pragma mark -
#pragma mark UIBarButtoItem Methods


- (IBAction)refreshButtonPressed:(id)sender
{
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [_btnRefresh setHidden:YES];
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    [self.titleView addSubview:refreshActivityView];
    
    _searchTxtField.text = @"";
    
    [appDelegate startIndicator];
    [self loadStores];
}


- (void)refreshTable {
    
    [appDelegate startIndicator];
    [self loadStores];
}



- (NSString *)kilometersFromPlace:(CLLocationCoordinate2D)from andToPlace:(CLLocationCoordinate2D)to {
    
    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:from.latitude longitude:from.longitude];
    CLLocation *storeLoc = [[CLLocation alloc] initWithLatitude:to.latitude longitude:to.longitude];
    
    CLLocationDistance distance = [userLoc distanceFromLocation:storeLoc]/1000;
    
    return [NSString stringWithFormat:@"%1f", distance];
}

//- (NSString *)setCurrentStatusForStore:(NSArray *)openingHours {
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"EEEE"];
//    NSString *today = [dateFormatter stringFromDate:[NSDate date]];
//    
//    [dateFormatter setDateFormat:@"HH:mm"];
//    NSString *strTime = [dateFormatter stringFromDate:[NSDate date]];
//    
//    if ([today isEqualToString:@"Monday"]) {
//        
//        NSString *storeTime = [openingHours objectAtIndex:0];
//        if (![storeTime isEqual:[NSNull null]]) {
//            NSString *currentStoreTiming = [self checkCurrentStateWithStoreTime:storeTime andDateFormatter:dateFormatter andCurrentTime:strTime];
//            return currentStoreTiming;
//        }
//        else {
//            return @"Closed";
//        }
//    }
//    else if ([today isEqualToString:@"Tuesday"]) {
//        
//        NSString *storeTime = [openingHours objectAtIndex:1];
//        if (![storeTime isEqual:[NSNull null]]) {
//            NSString *currentStoreTiming = [self checkCurrentStateWithStoreTime:storeTime andDateFormatter:dateFormatter andCurrentTime:strTime];
//            return currentStoreTiming;
//        }
//        else {
//            return @"Closed";
//        }
//    }
//    else if ([today isEqualToString:@"Wednesday"]) {
//        
//        NSString *storeTime = [openingHours objectAtIndex:2];
//        if (![storeTime isEqual:[NSNull null]]) {
//            NSString *currentStoreTiming = [self checkCurrentStateWithStoreTime:storeTime andDateFormatter:dateFormatter andCurrentTime:strTime];
//            return currentStoreTiming;
//        }
//        else {
//            return @"Closed";
//        }
//    }
//    else if ([today isEqualToString:@"Thursday"]) {
//        
//        NSString *storeTime = [openingHours objectAtIndex:3];
//        if (![storeTime isEqual:[NSNull null]]) {
//            NSString *currentStoreTiming = [self checkCurrentStateWithStoreTime:storeTime andDateFormatter:dateFormatter andCurrentTime:strTime];
//            return currentStoreTiming;
//        }
//        else {
//            return @"Closed";
//        }
//    }
//    else if ([today isEqualToString:@"Friday"]) {
//        
//        NSString *storeTime = [openingHours objectAtIndex:4];
//        if (![storeTime isEqual:[NSNull null]]) {
//            NSString *currentStoreTiming = [self checkCurrentStateWithStoreTime:storeTime andDateFormatter:dateFormatter andCurrentTime:strTime];
//            return currentStoreTiming;
//        }
//        else {
//            return @"Closed";
//        }
//    }
//    else if ([today isEqualToString:@"Saturday"]) {
//        
//        NSString *storeTime = [openingHours objectAtIndex:5];
//        if (![storeTime isEqual:[NSNull null]]) {
//            NSString *currentStoreTiming = [self checkCurrentStateWithStoreTime:storeTime andDateFormatter:dateFormatter andCurrentTime:strTime];
//            return currentStoreTiming;
//        }
//        else {
//            return @"Closed";
//        }
//    }
//    else if ([today isEqualToString:@"Sunday"]) {
//        
//        NSString *storeTime = [openingHours objectAtIndex:6];
//        if (![storeTime isEqual:[NSNull null]]) {
//            NSString *currentStoreTiming = [self checkCurrentStateWithStoreTime:storeTime andDateFormatter:dateFormatter andCurrentTime:strTime];
//            return currentStoreTiming;
//        }
//        else {
//            return @"Closed";
//        }
//    }
//    
//    return nil;
//}

- (NSString *)checkCurrentStateWithStoreTime:(NSString *)storeTime andDateFormatter:(NSDateFormatter *)dateFormatter andCurrentTime:(NSString *)timeNow
{
    NSArray *times = [storeTime componentsSeparatedByString:@"-"];
    NSString *strOpeningTime = [times objectAtIndex:0];
    NSString *strClosingTime = [times objectAtIndex:1];
    
    NSDate *openingTime = [dateFormatter dateFromString:strOpeningTime];
    NSDate *clossingTime = [dateFormatter dateFromString:strClosingTime];
    NSDate *currentTime = [dateFormatter dateFromString:timeNow];
    
    NSComparisonResult openingResult = [currentTime compare:openingTime];
    NSComparisonResult clossingResult = [currentTime compare:clossingTime];
    if (openingResult == NSOrderedSame || clossingResult == NSOrderedSame) {
        return @"Open";
    }
    else if (openingResult == NSOrderedAscending || clossingResult == NSOrderedAscending) {
        return @"Open";
    }
    else {
        return @"Closed";
    }
}

#pragma mark - TextField Delegates

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(get_SortedSearchArrayForText:) object:nil];
    NSString *textAfterUpdate = self.searchTxtField.text;
    textAfterUpdate = [textAfterUpdate stringByReplacingCharactersInRange:range withString:string];
    [self performSelector:@selector(get_SortedSearchArrayForText:) withObject:textAfterUpdate afterDelay:0.5];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return true;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//        [mo showHome];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end

