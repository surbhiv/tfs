//
//  NewsViewController.m
//  RConnect
//
//  Created by Hitesh Dhawan on 15/07/1937 SAKA.
//  Copyright © 1937 SAKA Neuronimbus. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsCell.h"
#import "TFS.pch"

@interface NewsViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource>
{
    UIRefreshControl *refreshControl;
    UIActivityIndicatorView *refreshActivityView;
}

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [self.collectionNews addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    
    [appDelegate startIndicator];
    [self loadNews];
}

- (IBAction)toggleSideMenuView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

//    [self.navigationController toggleSideMenuView];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"NEWS"],
                                     kFIRParameterItemName:@"NEWS",
                                     kFIRParameterContentType:@""
                                     }];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadNews {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *strURL = [NSString stringWithFormat:@"%@get_news", LOCAL_BASEURL];
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:strURL]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else {
        [appDelegate stopIndicator];

        _right_button.hidden = NO;
        [refreshActivityView removeFromSuperview];
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    
    [appDelegate stopIndicator];
    [refreshControl endRefreshing];
    _right_button.hidden = NO;
    [refreshActivityView removeFromSuperview];
    
    NSDictionary *dicNews = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicNews objectForKey:@"success"] integerValue] == 1) {

        self.arrNews = [[NSArray alloc] initWithArray:[dicNews objectForKey:@"message"]];
        [self.tblNews reloadData];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [refreshControl endRefreshing];
    _right_button.hidden = NO;
    [refreshActivityView removeFromSuperview];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];
}


#pragma mark
#pragma mark - TABLEVIEW
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 30;
    }
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerTitle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 200, 30)];
    title.textColor = [UIColor blackColor];
    title.font = [UIFont fontWithName:GeorgeMedium size:18.0];
    
    if (section == 0) {
        headerTitle.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0];
        title.text = @"Latest News";
    }
    else{
        headerTitle.backgroundColor = [UIColor clearColor];
        title.text = @"";
    }
    
    [headerTitle addSubview:title];
    
//    self.tblNews.tableHeaderView = headerTitle;  // for scrollable Header

    return  headerTitle;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_arrNews.count > 1) {
        return 2;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SingleNewsCell"];
        
        cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        cell.layer.shadowOpacity = 1;
        cell.layer.shadowOffset = CGSizeZero;
        cell.layer.shadowRadius = 3.5;
        cell.layer.masksToBounds = NO;
        
        UIImageView *backImage = (UIImageView *)[cell.contentView viewWithTag:9901];
        UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:9902];
        UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:9903];
        UILabel *descriptionLabel = (UILabel *)[cell.contentView viewWithTag:9904];
        UIView *outerView = (UIView *)[cell.contentView viewWithTag:9905];
        outerView.layer.cornerRadius = 8.0;
        outerView.clipsToBounds = YES;

        
        [backImage sd_setImageWithURL:[NSURL URLWithString:[[self.arrNews objectAtIndex:0] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        NSString *titleString = [[self.arrNews objectAtIndex:0] objectForKey:@"title"];
        if (![titleString isEqual:[NSNull null]]) {
            titleLabel.text = titleString;
        }
        
        NSString *publishDate = [[self.arrNews objectAtIndex:0] objectForKey:@"publish_date"];
        if (![publishDate isEqual:[NSNull null]]) {
            dateLabel.text = publishDate;
        }
        
        NSString *htmlString = [[self.arrNews objectAtIndex:0] objectForKey:@"short_description"];
        
        
        if (![htmlString isEqual:[NSNull null]]) {
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:14.0] range:NSMakeRange(0, attributedString.length)];
            
            descriptionLabel.attributedText = attributedString;
            descriptionLabel.numberOfLines = 3;
        }
        
        tableView.estimatedRowHeight = 340;
        tableView.rowHeight = UITableViewAutomaticDimension;
        
        return cell;
    }
    else{
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CollectionNewsCell"];

        _collectionNews = (UICollectionView *)[cell.contentView viewWithTag:9910];
        [_collectionNews reloadData];
        
        tableView.rowHeight = 300;
        
        return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        NewsDetailsViewController *detailsController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newsDetailsController"];
        detailsController.dicNews = [self.arrNews objectAtIndex:0];
        [self presentViewController:detailsController animated:NO completion:nil];
    }
}

#pragma mark
#pragma mark - CollectionView FlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
        return CGSizeMake(210, 285);
}




#pragma mark
#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.arrNews.count - 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"NewsCell";
    
    NewsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *outerView = (UIView *)[cell.contentView viewWithTag:9911];
    outerView.layer.cornerRadius = 8.0;
    outerView.clipsToBounds = YES;

    cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    cell.layer.shadowOpacity = 1;
    cell.layer.shadowOffset = CGSizeZero;
    cell.layer.shadowRadius = 3.5;
    cell.layer.masksToBounds = NO;
    
    [cell.imgNewsImage sd_setImageWithURL:[NSURL URLWithString:[[self.arrNews objectAtIndex:indexPath.row + 1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    NSString *titleString = [[self.arrNews objectAtIndex:indexPath.row + 1] objectForKey:@"title"];
    if (![titleString isEqual:[NSNull null]]) {
        cell.lblTitle.text = titleString;
    }
    
    NSString *publishDate = [[self.arrNews objectAtIndex:indexPath.row + 1] objectForKey:@"publish_date"];
    if (![publishDate isEqual:[NSNull null]]) {
        cell.lblPublishDate.text = publishDate;
    }
    
    NSString *htmlString = [[self.arrNews objectAtIndex:indexPath.row + 1] objectForKey:@"short_description"];
    
    
    if (![htmlString isEqual:[NSNull null]]) {
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:14.0] range:NSMakeRange(0, attributedString.length)];
        
        cell.lblDescription.attributedText = attributedString;
        cell.lblDescription.numberOfLines = 2;
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES  ];

    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    NewsDetailsViewController *detailsController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newsDetailsController"];
    detailsController.dicNews = [self.arrNews objectAtIndex:indexPath.row + 1];
    [self presentViewController:detailsController animated:NO completion:nil];
}


#pragma mark -
#pragma mark UIBarButtoItem Methods

- (IBAction)refreshButtonPressed:(id)sender
{
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = _right_button.frame;
    }
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    [self.titleView addSubview:refreshActivityView];
    
    
    _right_button.hidden = YES;
    
    [appDelegate startIndicator];
    [self loadNews];
}


- (void)refreshTable
{
    [self loadNews];
}


- (NSDate *)convertStringToDate:(NSString *)dateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *newsDate = [formatter dateFromString:dateString];
    
    return newsDate;
}

- (NSString *)convetDateToString:(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//        [mo showHome];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
