//
//  MoreViewController.h
//  k kiosk
//
//  Created by HiteshDhawan on 23/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreViewController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) UILabel *screenNumber;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNo;
@property (weak, nonatomic) IBOutlet UITextField *txtDateOfBirth;
@property (weak, nonatomic) IBOutlet UIView *viewProfile;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtPin;

@property (weak, nonatomic) IBOutlet UIButton *btnSaveChanges;



//@property (weak, nonatomic) IBOutlet UIView *viewContact;

//@property (weak, nonatomic) IBOutlet UIButton *emailButton;
//@property (weak, nonatomic) IBOutlet UIButton *contactButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightProfleButtonConstraint;

//- (IBAction)logoutButtonPressed:(id)sender;
//- (IBAction)dateOfBirthButtonPressed:(id)sender;
- (IBAction)saveChangesButtonPressed:(id)sender;

//- (IBAction)NUMBERPressed:(id)sender;
//- (IBAction)EMAILPressed:(id)sender;

@end
