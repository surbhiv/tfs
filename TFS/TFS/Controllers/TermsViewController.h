//
//  TermsViewController.h
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIWebView *twebView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property BOOL isloggedin;

@property (weak, nonatomic) IBOutlet UIView *imgHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;


@end
