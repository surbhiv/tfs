//
//  ReferViewController.m
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "ReferViewController.h"
#import "ContactViewController.h"
#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>

@interface ReferViewController ()<MFMessageComposeViewControllerDelegate,UITextFieldDelegate>
{
    UIPickerView *countryPicker;
    NSString *smsTemplateStrng;
}
@end


@implementation ReferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCountryPickerView];
    

    \
    // Fetch list of phone code
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryPhoneCode" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    self.arrCountryCode = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] objectForKey:@"countries"];
    
    // Dismiss keyboard gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    // Ask contacts permission
    [self askContactsPermission];
    
    self.contactPicker = [[CNContactPickerViewController alloc] init];
    self.contactPicker.delegate = self;
    
    _viewmobileu_Share.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _viewmobileu_Share.layer.borderWidth = 2.0f;

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_isShareView) {
        self.shareView.hidden = NO;
        self.referView.hidden = YES;
        self.view.backgroundColor = [UIColor clearColor];
        [self.view bringSubviewToFront:self.shareView];
    }
    else{
        self.referView.hidden = NO;
        self.shareView.hidden = YES;
        self.view.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
        [self.view bringSubviewToFront:self.referView];
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    if (_isShareView) {
        [self performSelector:@selector(PostNotificationForUpdation) withObject:nil afterDelay:3.0];
    }
}


-(void)PostNotificationForUpdation{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SharingCompleted" object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


#pragma mark -
#pragma mark Custom Methods

- (void)dismissKeyboard
{
    [self.txtMobile resignFirstResponder];
    [self.txtMobile_Share resignFirstResponder];
}

- (BOOL)validateMobileNumber:(NSString *)mobileNumber
{
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    BOOL matched = [mobileNumberPred evaluateWithObject:mobileNumber];
    return matched;
}

- (void)addCountryPickerView {
    
    countryPicker = [[UIPickerView alloc] init];
    countryPicker.dataSource = self;
    countryPicker.delegate = self;
    countryPicker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-countryPicker.frame.size.height-50, 320, 50)];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    self.txtPhoneCode.inputView = countryPicker;
    self.txtPhoneCode.inputAccessoryView = toolBar;
    
    self.txtphoneCode_Share.inputView = countryPicker;
    self.txtphoneCode_Share.inputAccessoryView = toolBar;
    
    self.txtphoneCode_Share.text = @"+91";

}

- (void)done:(id)sender {
        
    [self.txtPhoneCode resignFirstResponder];
    [self.txtphoneCode_Share resignFirstResponder];
}

- (void)askContactsPermission {
    
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            ABAddressBookRef addressBook = ABAddressBookCreate();
//            NSLog(@"Created address book ref: %@", addressBook);
        });
    }
}

#pragma mark
#pragma mark - ACTIONS


- (IBAction)cancelButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
//    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//    int lastSelectedIndex = (int)mo.LastselectedIndexPath.row;
//    if (lastSelectedIndex == 10) {
//        [mo showHome];
//    }
//    else{
//        int new = (int)mo.LastselectedIndexPath.row;
//        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:new inSection:0]];
//    }
}

- (IBAction)sendButtonPressed:(id)sender {
    
    [self.txtMobile resignFirstResponder];
    
    if (self.txtMobile.text.length > 0) {
        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobile.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        
        [SVProgressHUD showWithStatus:@"Validating..." maskType:SVProgressHUDMaskTypeGradient];
        NSString *strURL = [NSString stringWithFormat:@"%@ValidReferral/%@/%@", BASEURL, appDelegate.strUserId, strMobileNumber];
        [self referFriend:strURL];
    }
    else{
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please enter your Mobile Number!"];
    }

}

- (IBAction)contactsButtonPressed:(id)sender {
    
    [self presentViewController:self.contactPicker animated:YES completion:nil];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_txtMobile] || [textField isEqual:_txtMobile_Share]) {
        if (range.location > 9) {
            return NO;
        }
    }
    return YES;
}



#pragma mark -
#pragma mark UIPickerViewDataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.arrCountryCode count];
}

#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)referFriend:(NSString *)urlString {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobile.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, strMobileNumber, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        request.name = @"REFERINGFRIEND";
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [SVProgressHUD dismiss];
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    
    NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];

    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"REFER FRIEND"],
                                     kFIRParameterItemName:@"REFER FRIEND",
                                     kFIRParameterContentType:@""
                                     }];

    
    if ([request.name isEqualToString:@"REFERINGFRIEND"]) {
        
        if ([[dicUser allKeys] containsObject:@"SmsTemplate"]) {
            
            smsTemplateStrng = [dicUser valueForKey:@"SmsTemplate"];
            [self SendRefferalMethod];
            
        }
        else {
            
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Referral Error!" andMessage:@"Your referral mobile number is already registered. Kindly try a different mobile number." andButtonTitle:@"Dismiss"];
        }
    }
    else if ([request.name isEqualToString:@"REFERINGCHECK"]){
        if (dicUser.count == 0)
        {
            [SVProgressHUD dismiss];
            NSString *strPhoneCode = self.txtPhoneCode.text;
            if ([strPhoneCode hasPrefix:@"+"]) {
                strPhoneCode = [strPhoneCode substringFromIndex:1];
            }
            NSString *strMobileNumber =self.txtMobile.text;
            
            // Send message
            [self prepareMessage:smsTemplateStrng andMobileNumber:strMobileNumber];
        }
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Referral Error!" andMessage:@"Your referral mobile number is already registered. Kindly try a different mobile number." andButtonTitle:@"Dismiss"];
    }
    
}


-(void)SendRefferalMethod{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
        
        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobile.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        
        NSString *urlString = [NSString stringWithFormat:@"%@referral", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, strMobileNumber, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        request.name = @"REFERINGCHECK";

        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"UserID":appDelegate.strUserId,
                                   @"MSN":strMobileNumber};
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request startAsynchronous];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }

}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];
}

#pragma mark -
#pragma mark UIPickerViewDelegate Methods

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    return [NSString stringWithFormat:@"%@      %@", [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"], [[self.arrCountryCode objectAtIndex:row] objectForKey:@"name"]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (_isShareView) {
        self.txtphoneCode_Share.text = [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"];
    }
    else{
        self.txtPhoneCode.text = [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"];
    }
}


#pragma mark -
#pragma mark ContactViewControllerDelegate Methods

- (void)selectedPersonContact:(NSString *)phoneNumber
{
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSString *firstChar = [phoneNumber substringToIndex:1];
    if ([firstChar isEqualToString:@"+"]) {
        
        phoneNumber = [phoneNumber substringFromIndex:3];
    }
    
    if (_isShareView) {
        self.txtMobile_Share.text = phoneNumber;
    }
    else{
        self.txtMobile.text = phoneNumber;
    }
}

- (void)prepareMessage:(NSString *)msg andMobileNumber:(NSString *)number {
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    NSArray* arrayOfStrings = [number componentsSeparatedByString:@""];
    
    picker.recipients = arrayOfStrings;
    picker.body = msg;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
            
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Failed to send SMS!" andButtonTitle:@"OK"];
            break;
        }
            
        case MessageComposeResultSent:
        {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"" andMessage:@"Sent SMS successfully." andButtonTitle:@"OK"];
            break;
        }
            
        default:
            break;
    }
    
    [controller removeFromParentViewController];
    [controller dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark CNContactPickerDelegate Methods

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty {
    
    NSString *phoneNumber = [NSString stringWithFormat:@"%@", [[contactProperty value] stringValue]];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSString *firstChar = [phoneNumber substringToIndex:1];
    if ([firstChar isEqualToString:@"+"]) {
        phoneNumber = [phoneNumber substringFromIndex:3];
    }
    
    if (_isShareView) {
        self.txtMobile_Share.text = phoneNumber;
    }
    else{
        self.txtMobile.text = phoneNumber;
    }
}

- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
    
//    NSLog(@"ContactPickerDidCancel");
}


-(void)setImageOntextField:(UITextField *)textfield andImage:(NSString *)image withSize:(CGSize)size{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, size.width, size.height)];
    imv.image = [UIImage imageNamed:image];
    textfield.rightViewMode = UITextFieldViewModeAlways;
    textfield.rightView = imv;
}



#pragma mark
#pragma mark
#pragma mark - SHARE View



- (IBAction)ShareButtonButtonPressed:(id)sender {
    if (self.txtMobile_Share.text.length > 0) {
        [self ShareAPICalled];
    }
    else{
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please enter your Mobile Number!"];
    }
}

-(void)ShareAPICalled{

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status ) {
        
        [SVProgressHUD showWithStatus:@"Please Wait..." maskType:SVProgressHUDMaskTypeGradient];
        
        NSString *strPhoneCode = self.txtphoneCode_Share.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobile_Share.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];

        
        NSString *urlString = [NSString stringWithFormat:@"%@coupon/%@/%@/%@/%@", BASEURL, appDelegate.strUserId,self.campeignID,self.couponID,strMobileNumber];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, self.couponID,SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(SHARErequestFinished:)];
        [request setDidFailSelector:@selector(SHARErequestFailed:)];
        
        [request startAsynchronous];
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
    
}



- (void)sharingView:(NSString *)text
{
    NSMutableArray *sharingItems = [[NSMutableArray alloc]initWithObjects:text, nil];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:^{
        
        [self CancelSharePressed:nil];
    }];
}




- (void)SHARErequestFinished:(ASIHTTPRequest *)request {
    
    [SVProgressHUD dismiss];
    
    NSDictionary *responseDict = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    NSString *responseCode = responseDict[@"ResultCode"];
    NSString *responseMessage = responseDict[@"ResultMessage"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"SHARE COUPON"],
                                     kFIRParameterItemName:@"SHARE COUPON",
                                     kFIRParameterContentType:@""
                                     }];

    
    if ([responseCode isEqualToString: @"SharedByUrl"] || [responseCode isEqualToString:@"SharedOnSms"])
    {
        NSString *mssg = responseDict[@"Message"];
        // create UIACTIVITY CONTROLLER FOR SHARING THE LINK
        [self sharingView: mssg];
    }
    else if ([responseCode isEqualToString:@"SharedByApp"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:responseCode message:responseMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             [self.view removeFromSuperview];
                                                             [self removeFromParentViewController];
                                                         }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:Nil];

    }
    else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:responseCode message:responseMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             [self.view removeFromSuperview];
                                                             [self removeFromParentViewController];
                                                         }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:Nil];
    }
    
}

- (void)SHARErequestFailed:(ASIHTTPRequest *)request {
    
    [SVProgressHUD dismiss];
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];
}



- (IBAction)CancelSharePressed:(id)sender {

    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        [mo showHome];
    }
}




@end
