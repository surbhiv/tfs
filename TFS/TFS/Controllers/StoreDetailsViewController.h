//
//  StoreDetailsViewController.h
//  RConnect
//
//  Created by Hitesh Dhawan on 06/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;
@import GoogleMaps;


@interface StoreDetailsViewController : UIViewController<CLLocationManagerDelegate>

@property (strong,nonatomic) NSDictionary *receivedDictionary;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;


@property (weak, nonatomic) IBOutlet UIButton *bttnSlideView;
@property (weak, nonatomic) IBOutlet GMSMapView *viewGoogleMaps;
@property (weak, nonatomic) IBOutlet UIView *viewStoreDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTime1;
@property (weak, nonatomic) IBOutlet UILabel *lblTime2;
@property (weak, nonatomic) IBOutlet UILabel *lblTime3;
@property (weak, nonatomic) IBOutlet UILabel *lblTime4;
@property (weak, nonatomic) IBOutlet UILabel *lblTime5;
@property (weak, nonatomic) IBOutlet UILabel *lblTime6;
@property (weak, nonatomic) IBOutlet UILabel *lblTime7;
@property (weak, nonatomic) IBOutlet UITextView *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentState;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAverage;
@property (weak, nonatomic) IBOutlet UIImageView *imgBrand;


@property (weak, nonatomic) IBOutlet UIView *callView;
@property (weak, nonatomic) IBOutlet UIView *scheduleView;
@property (weak, nonatomic) IBOutlet UIView *noteView;


- (IBAction)backButtonPressed:(id)sender;

@end
