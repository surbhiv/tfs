//
//  StoreDetailsViewController.m
//  RConnect
//
//  Created by Hitesh Dhawan on 06/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "StoreDetailsViewController.h"
#import "TFS.pch"


@interface StoreDetailsViewController ()<GMSMapViewDelegate>
{
    CGFloat _lastContentOffset;
    GMSMarker *markerStore;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation StoreDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadAverageRating];
    [self setAppearance];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"STORE DETAIL"],
                                     kFIRParameterItemName:@"STORE DETAIL",
                                     kFIRParameterContentType:@""
                                     }];

}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    
    for (ASIHTTPRequest *request in ASIHTTPRequest.sharedQueue.operations) {
        if (![request isCancelled]) {
            [request cancel];
            [request setDelegate:nil];
        }
    }
}

#pragma mark - Open Google Map
-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]) {
        
        NSString *addSre = [NSString stringWithFormat:@"comgooglemaps://?daddr=%@,%@&directionsmode=transit",[self.receivedDictionary valueForKey:@"Latitude"],[self.receivedDictionary valueForKey:@"Longitude"]];//@"comgooglemaps://?center=%f,%f&zoom=12"//daddr
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:addSre]];
    }
    else {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Map Error" andMessage:@"Google maps not installed"];
    }
    return  true;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)slideButtonPressed:(id)sender
{
    self.bttnSlideView.selected = !self.bttnSlideView.isSelected;
    [self slideView];
}

- (void)setAppearance
{
    // Bring Slide Button Up
    [self.viewGoogleMaps bringSubviewToFront:self.bttnSlideView];
    
    // Making Slide Button SemiCircular
    [self.bttnSlideView.layer setCornerRadius:self.bttnSlideView.frame.size.width/2];
    
    // Setting Location co-ordinates for Map View
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([[self.receivedDictionary valueForKey:@"Latitude"] floatValue], [[self.receivedDictionary valueForKey:@"Longitude"] floatValue]);
    
    // Configuring Maps for Use
    [self.viewGoogleMaps animateToLocation:location];
    [self.viewGoogleMaps animateToZoom:12];
    self.viewGoogleMaps.myLocationEnabled=YES;
    self.viewGoogleMaps.settings.compassButton=YES;
    self.viewGoogleMaps.settings.scrollGestures=YES;
    self.viewGoogleMaps.settings.rotateGestures=YES;
    self.viewGoogleMaps.settings.zoomGestures=YES;
    self.viewGoogleMaps.settings.indoorPicker=YES;
    self.viewGoogleMaps.settings.consumesGesturesInView=YES;
    self.viewGoogleMaps.settings.tiltGestures=YES;
//    self.viewGoogleMaps.settings.accessibilityPerformMagicTap
    self.viewGoogleMaps.delegate = self;
    
    
    // Setting Up Marker View
    markerStore = [[GMSMarker alloc] init];
    markerStore.position = location;
    markerStore.icon = [UIImage imageNamed:@"locate"];
    
    
    // Setting Title To Marker View
    markerStore.title = [NSString stringWithFormat:@"Location"];
    markerStore.snippet = [NSString stringWithFormat:@"%@",[self.receivedDictionary valueForKey:@"Name"]];
    
    markerStore.map = self.viewGoogleMaps;
    [self.viewGoogleMaps setSelectedMarker:markerStore];
    
    
//    NSString *storeImg = [self.receivedDictionary valueForKey:@"Logo"];
//    if (![storeImg isEqual:[NSNull null]]) {
//        [self.imgBrand sd_setImageWithURL:[NSURL URLWithString:storeImg]
//                     placeholderImage:nil];//[UIImage imageNamed:@"logoBig.png"]
//    }
//    else{
        self.imgBrand.image = [UIImage imageNamed:@"logoBig.png"];
        
//    }
    
    // Assigning values to labels through API call
    self.lblAddress.text = [self.receivedDictionary valueForKey:@"Address"];
    self.lblTitle.text = [self.receivedDictionary valueForKey:@"Name"];
    
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:0] != [NSNull null]) {
        self.lblTime1.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:0];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:1] != [NSNull null]) {
        self.lblTime2.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:1];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:2] != [NSNull null]) {
        self.lblTime3.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:2];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:3] != [NSNull null]) {
        self.lblTime4.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:3];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:4] != [NSNull null]) {
        self.lblTime5.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:4];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:5] != [NSNull null]) {
        self.lblTime6.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:5];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:6] != [NSNull null]) {
        self.lblTime7.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:6];
    }
    
    UIFont *attFont = [UIFont fontWithName:GeorgeRegular size:13.0];
    if ([self.receivedDictionary objectForKey:@"Telephone"] == nil || [[self.receivedDictionary objectForKey:@"Telephone"] isEqualToString:@""]) {
        
    }
    else if (attFont ==  nil){
        self.txtPhoneNumber.attributedText = [[NSAttributedString alloc]initWithString:[self.receivedDictionary objectForKey:@"Telephone"] attributes:@{NSForegroundColorAttributeName : THEMECOLOR, NSFontAttributeName : [UIFont fontWithName:HelveticaNeueRegular size:13.0],  NSUnderlineColorAttributeName : THEMECOLOR, NSUnderlineStyleAttributeName :@(NSUnderlineStyleSingle)}];
    }
    else{
        self.txtPhoneNumber.attributedText = [[NSAttributedString alloc]initWithString:[self.receivedDictionary objectForKey:@"Telephone"] attributes:@{NSForegroundColorAttributeName : THEMECOLOR, NSFontAttributeName : [UIFont fontWithName:GeorgeRegular size:13.0],  NSUnderlineColorAttributeName : THEMECOLOR, NSUnderlineStyleAttributeName :@(NSUnderlineStyleSingle)}];
    }
    
    self.lblSubtitle.text = [self.receivedDictionary objectForKey:@"ShortName"];
    self.lblNote.text = [self.receivedDictionary objectForKey:@"Note"];
    self.lblDistance.text = [NSString stringWithFormat:@"%.2f km", [[self.receivedDictionary objectForKey:@"Distance"] floatValue]];
    
    if ([[self.receivedDictionary objectForKey:@"CurrentState"] isEqualToString:@"Open"]) {
        self.lblCurrentState.text = [self.receivedDictionary objectForKey:@"CurrentState"];
        self.lblCurrentState.textColor = [UIColor blackColor];
    }
}


#pragma mark -
#pragma mark ConfiguringSlideView Methods

- (void)slideView
{
    CGPoint topOffset = CGPointMake(0,0);
    
    if (self.scrollView.contentOffset.x == topOffset.x && self.scrollView.contentOffset.y == topOffset.y) {
        
        [self.scrollView setContentOffset:CGPointMake(0, self.viewStoreDetails.frame.origin.y - 50) animated:YES];
    }
    else {
        
        [self.scrollView setContentOffset:topOffset animated:YES];
    }
}


#pragma mark - 
#pragma mark UIScrollViewDelegate Methdods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    _lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (_lastContentOffset > scrollView.contentOffset.y) {
        self.bttnSlideView.selected = NO;
    }
    else {
        self.bttnSlideView.selected = NO;
    }
}

- (IBAction)backButtonPressed:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadAverageRating {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *appId = @"1234";
        NSString *pCat = @"SHOPEXPERIENCE";
        
        NSString *urlString = [NSString stringWithFormat:@"%@StoreStats/%@/%@/%@/%@", BASEURL, appDelegate.strUserId, appId, [_receivedDictionary valueForKey:@"Id"], pCat];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@%@%@", _dateString, appDelegate.strUserId, appId, [_receivedDictionary valueForKey:@"Id"], pCat, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source md5Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-DATE" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:@"There is no network, please check your internet." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    NSDictionary *dicRating = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicRating objectForKey:@"AvRating"] integerValue] > 0) {
        
        for (int i=100; i<=104; i++) {
            UIImageView *imgView = (UIImageView *)[self.view viewWithTag:i];
            CGFloat rate = [[dicRating objectForKey:@"AvRating"] floatValue];

            NSString *str=[NSString stringWithFormat:@"%f",rate];
            NSArray *arr=[str componentsSeparatedByString:@"."];
            int tempInt=[[arr lastObject] intValue];

            if (i < 100+[[dicRating objectForKey:@"AvRating"] integerValue]) {
                    [imgView setImage:[UIImage imageNamed:@"star_red"]];
            }
            else if (i == 100+[[dicRating objectForKey:@"AvRating"] integerValue]){
                if (tempInt <= 500000 && tempInt != 000000) {
                    [imgView setImage:[UIImage imageNamed:@"star_redHalf"]];
                }
                else if (tempInt == 0){
                    [imgView setImage:[UIImage imageNamed:@"star"]];
                }
                else{
                    [imgView setImage:[UIImage imageNamed:@"star_red"]];
                }
            }
            else {
                [imgView setImage:[UIImage imageNamed:@"star"]];
            }
        }
    }
    
    if ([[dicRating objectForKey:@"TotRatings"] integerValue] > 0) {
        _lblTotalAverage.text = [NSString stringWithFormat:@"%ld", [[dicRating objectForKey:@"TotRatings"] integerValue]];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error!" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];

}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//        [mo showHome];
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        mo.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

@end
