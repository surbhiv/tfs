//
//  TermsViewController.m
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "TermsViewController.h"
#import "TFS.pch"


@interface TermsViewController ()
{
    UIActivityIndicatorView *refreshActivityView;

}
@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.twebView.layer.cornerRadius = 7.0;
    self.twebView.clipsToBounds = YES;
    
    self.twebView.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.twebView.layer.shadowOpacity = 1;
    self.twebView.layer.shadowOffset = CGSizeZero;
    self.twebView.layer.shadowRadius = 2.5;
    self.twebView.layer.masksToBounds = NO;

    
    [self.activityIndicator setColor:[UIColor grayColor]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@get_content?alias=term_conditions", LOCAL_BASEURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [self.twebView loadRequest:request];
    [self.twebView setDelegate:self];
    
//    if (_isloggedin == YES) {
//        [_menuButton setImage:[UIImage imageNamed:@"SideMenuIcon"] forState:UIControlStateNormal];
//    }
//    else{
        [_menuButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)toggleSideMenuView:(id)sender
{
//    if (_isloggedin == YES) {
//         [self toggleSideMenuView];
//    }
//    else{
        [self dismissViewControllerAnimated:YES completion:nil];
//    }
}


#pragma mark -
#pragma mark UIBarButtoItem Methods


- (IBAction)refreshButtonPressed:(id)sender
{
    
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    [self.imgHeader addSubview:refreshActivityView];
    _btnRefresh.hidden = YES;
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@get_content?alias=term_conditions", LOCAL_BASEURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [self.twebView loadRequest:request];
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [appDelegate startIndicator];
    _btnRefresh.hidden = YES;
    
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [appDelegate stopIndicator];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    
    [self.activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:( NSError *)error {
    
    [appDelegate stopIndicator];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    
    [self.activityIndicator stopAnimating];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
//        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
//        [mo showHome];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
