//
//  NewsDetailsViewController.h
//  RConnect
//
//  Created by HiteshDhawan on 31/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailsViewController : UIViewController <UITextViewDelegate>

@property (nonatomic, strong) NSDictionary *dicNews;

@property (weak, nonatomic) IBOutlet UIImageView *imgNewsImage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPublishDate;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UIView *contentView;


- (IBAction)backButtonPressed:(id)sender;

@end
