//
//  ContactUsVC.m
//  TFS
//
//  Created by Surbhi on 26/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import "ContactUsVC.h"
#import "TFS.pch"

//#define phoneNumber @"+919910615909"
#define phoneNumber @"+917827247247" // TFS
//#define emailID @"abc@gmail.com"
#define emailID @"feedback-tfs@modi.com" //TFS

@interface ContactUsVC ()<UIWebViewDelegate, MFMailComposeViewControllerDelegate>

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.contactButton setAttributedTitle:[[NSAttributedString alloc]initWithString:phoneNumber attributes:@{NSForegroundColorAttributeName : THEMECOLOR, NSFontAttributeName : [UIFont fontWithName:GeorgeSemiBold size:16.0],  NSUnderlineColorAttributeName : THEMECOLOR, NSUnderlineStyleAttributeName :@(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    
    [self.emailButton setTitle:emailID forState:UIControlStateNormal];

    self.viewContact.layer.cornerRadius = 3.0;
    self.viewContact.clipsToBounds = YES;
    self.viewContact.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.viewContact.layer.shadowOpacity = 1;
    self.viewContact.layer.shadowOffset = CGSizeZero;
    self.viewContact.layer.shadowRadius = 1.0;
    self.viewContact.layer.masksToBounds = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)toggleSideMenuView:(id)sender
{
    //    [self.navigationController toggleSideMenuView];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)NUMBERPressed:(id)sender{
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phoneNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Call facility is not available!!!"andButtonTitle:@"Ok"];
    }
    
    
    
}

- (IBAction)EMAILPressed:(id)sender;{
    
    NSString *app_version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *device_version = [UIDevice currentDevice].systemVersion;
    NSString *device_name = [UIDevice currentDevice].model;

    NSString *emailTitle = [NSString stringWithFormat:@"24 SEVEN (%@), %@ - %@",app_version,device_name,device_version];
    //@"24 SEVEN HELPDESK";
    // Email Content
    
    //telephone number // date // device id
    
    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"MSN"]);

    NSString *phone = [[NSUserDefaults standardUserDefaults] valueForKey:@"MSN"];
    NSString *dob = [[NSUserDefaults standardUserDefaults] valueForKey:@"DateOfBirth"];
    NSString *messageBody;
    
    if (dob != nil || ![dob isEqualToString:@""]) {
        messageBody = [NSString stringWithFormat:@"\n\n\nDEVICE ID : %@\nMobile Number : %@\nD.O.B : %@",appDelegate.strDeviceToken,phone,dob];
    }
    else{
        messageBody = [NSString stringWithFormat:@"\n\n\nDEVICE ID : %@\nMobile Number : %@",appDelegate.strDeviceToken,phone];
    }
    
    NSArray *toRecipents = [NSArray arrayWithObject:[NSString stringWithFormat:@"%@",emailID]];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)FBPressed:(id)sender;{
    
    NSURL *url = [NSURL URLWithString:@"https://www.facebook.com/24SevenIN/"];
//    [[UIApplication sharedApplication] openURL:url];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Oops!\n Some error occurred.\n Please try again later."andButtonTitle:@"Ok"];
    }
}


- (IBAction)TwitterPressed:(id)sender;{
   
    NSURL *url = [NSURL URLWithString:@"https://twitter.com/24sevenin"];
    //    [[UIApplication sharedApplication] openURL:url];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Oops!\n Some error occurred.\n Please try again later."andButtonTitle:@"Ok"];
    }
}



- (IBAction)InstaGramPressed:(id)sender;{
    NSURL *url = [NSURL URLWithString:@"https://www.instagram.com/24sevenin/"];
    //    [[UIApplication sharedApplication] openURL:url];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Oops!\n Some error occurred.\n Please try again later."andButtonTitle:@"Ok"];
    }
    
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        //        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        //        [mo showHome];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
