//
//  StoresViewController.h
//  RConnect
//
//  Created by HiteshDhawan on 27/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoresViewController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) UILabel *screenNumber;

@property (nonatomic, strong) NSArray *arrStores;
@property (nonatomic, strong) NSMutableArray *arr_SearchStores;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;


@property BOOL isDirectView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (nonatomic, weak) IBOutlet UITableView *tblStores;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeight;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;

- (IBAction)refreshButtonPressed:(id)sender;

@end
