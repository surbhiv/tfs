//
//  AppDelegate.h
//  TFS
//
//  Created by Surbhi on 01/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@import GoogleMaps;
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *strUserId;
@property (strong, nonatomic) NSDictionary *dicOffers;
@property (strong, nonatomic) NSString *strDeviceToken;
@property (strong, nonatomic) NSString *strMobileNumber;
@property (assign, nonatomic) BOOL isLoader;
@property (assign, nonatomic) BOOL isRegistered;

@property (assign, nonatomic) BOOL viewOnceLoaded;
@property (assign, nonatomic) BOOL justLogedIn;

@property UIView *activityView1;
@property UIView *activityView2;
@property UIImageView *imageForR;
@property UIView *logoView;
@property UIImageView *imageText;



- (NSString *)getUniqueDeviceIdentifierAsString;
- (void)loadFeedbackBacklogValue;
- (void)changeRootViewController;
- (void)registerDevice;
- (void)loadStores;
- (void)getUser;
- (void) registerForRemoteNotifications;
- (void) unregisterForRemoteNotifications;

@property (strong, nonatomic) NSMutableArray *geofences;
@property (strong, nonatomic) NSMutableArray *latestGeofences;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSArray *arrStores;
@property (assign, nonatomic) int backlogVal;

-(void)setTopViewwithVC:(id)viewControl;
-(id)returnTopViewController;
+ (AppDelegate *) sharedInstance;
-(void)startIndicator;
-(void)stopIndicator;

-(void)showWelcomePop:(NSString *)name;
-(void)hideWelcomePop;

@end

